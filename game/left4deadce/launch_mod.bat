@echo off
setlocal

set extraparams="%1"
set extraparams=%extraparams:"=%
if "%extraparams%" == "" (
	set extraparams=
)

set params=^
-steam -override_vpk^
 -w 1600 -h 900 -windowed^
 -high -threads 8^
 -nocrashdialog -hushasserts -hushsteam -nobreakpad -noassert -nominidumps^
 -nop4 -novid -nojoy -nohltv -nogameui -nogamestats^
 -allowspectators^
 %extraparams%

tools/launch_tool.bat "swarm.exe" "%params%" "%ProgramFiles(x86)%\Steam\steamapps\common\Alien Swarm"

endlocal
exit