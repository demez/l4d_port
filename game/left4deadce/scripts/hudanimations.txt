// sample animation script
//
//
// commands:
//	Animate <panel name> <variable> <target value> <interpolator> <start time> <duration>
//		variables:
//			FgColor
//			BgColor
//			Position
//			Size
//			Blur		(hud panels only)
//			TextColor	(hud panels only)
//			Ammo2Color	(hud panels only)
//			Alpha		(hud weapon selection only)
//			SelectionAlpha  (hud weapon selection only)
//			TextScan	(hud weapon selection only)
//
//		interpolator:
//			Linear
//			Accel - starts moving slow, ends fast
//			Deaccel - starts moving fast, ends slow
//
//	RunEvent <event name> <start time>
//		starts another even running at the specified time
//
//	StopEvent <event name> <start time>
//		stops another event that is current running at the specified time
//
//	StopAnimation <panel name> <variable> <start time>
//		stops all animations refering to the specified variable in the specified panel
//
//	StopPanelAnimations <panel name> <start time>
//		stops all active animations operating on the specified panel
//
//
// Useful game console commands:
//	cl_Animationinfo <hudelement name> or <panelname> 
//		displays all the animatable variables for the hud element
//


event LevelInit
{
}

event FadeOutTeamLine
{
	// make the display visible
	Animate TeamDisplay Alpha 		"0"		Linear 0.0 0.25
}

event FadeInTeamLine
{
	// make the display visible
	Animate TeamDisplay Alpha 		"255"		Linear 0.0 0.5
}

event OpenWeaponSelectionMenu
{
	StopEvent CloseWeaponSelectionMenu	0.0
	StopEvent WeaponPickup				0.0
	StopEvent FadeOutWeaponSelectionMenu	0.0

	// make the display visible
	Animate HudWeaponSelection Alpha 		"128"		Linear 0.0 0.1
	Animate HudWeaponSelection SelectionAlpha 	"255"		Linear 0.0 0.1
	Animate HudWeaponSelection FgColor		"FgColor_HL2"	Linear 0.0 0.1
	Animate HudWeaponSelection TextColor		"BrightFg_HL2"	Linear 0.0 0.1
	Animate HudWeaponSelection TextScan		"1" 		Linear 0.0 0.1
}

event CloseWeaponSelectionMenu
{
	// hide the whole thing near immediately
	Animate HudWeaponSelection FgColor		"0 0 0 0"	Linear 0.0 0.1
	Animate HudWeaponSelection TextColor		"0 0 0 0"	Linear 0.0 0.1
	Animate HudWeaponSelection Alpha		"0" 		Linear 0.0 0.1
	Animate HudWeaponSelection SelectionAlpha 	"0" 		Linear 0.0 0.1
}

event FadeOutWeaponSelectionMenu
{
	// slowly hide the whole thing
	Animate HudWeaponSelection FgColor		"0 0 0 0"	Linear 0.0 1.5
	Animate HudWeaponSelection TextColor		"0 0 0 0"	Linear 0.0 1.5
	Animate HudWeaponSelection Alpha		"0" 		Linear 0.0 1.5
	Animate HudWeaponSelection SelectionAlpha 	"0" 		Linear 0.0 1.5
}


// ammo has been picked up
event AmmoIncreased
{
	Animate HudAmmo		FgColor	"BrightFg_HL2"		Linear	0.0	0.15
	Animate HudAmmo		FgColor	"FgColor_HL2"		Deaccel	0.15	1.5
	Animate HudAmmo		Blur		"5"			Linear	0.0	0.0 
	Animate HudAmmo		Blur		"0"			Accel		0.01	1.5 
}

// ammo has been decreased, but there is still some remaining
event AmmoDecreased
{
	StopEvent AmmoIncreased	0.0
	
	Animate HudAmmo		Blur		"7"			Linear	0.0	0.0
	Animate HudAmmo		Blur		"0"			Deaccel	0.1	1.5
	
	Animate HudAmmo		TextColor	"BrightFg_HL2"		Linear	0.0	0.1
	Animate HudAmmo		TextColor	"FgColor_HL2"		Deaccel	0.1	0.75
}

// primary ammo is zero
event AmmoEmpty
{
	Animate Hudammo	FgColor		"BrightDamagedFg_HL2"	Linear	0.0	0.2
	Animate Hudammo	FgColor		"DamagedFg_HL2"		Accel		0.2	1.2		
}

// current weapon has been changed
event WeaponChanged
{
	Animate HudAmmo		BgColor		"250 220 0 80"	Linear	0.0		0.1
	Animate HudAmmo		BgColor		"BgColor_HL2"		Deaccel	0.1		1.0
	Animate HudAmmo		FgColor		"BrightFg_HL2"		Linear	0.0		0.1
	Animate HudAmmo		FgColor		"FgColor_HL2"		Linear	0.2		1.5
}

// ran if we just changed to a weapon that needs clip ammo
event WeaponUsesClips
{
	Animate HudAmmo		Position	"r150 432"	Deaccel	0.0		0.4
	Animate HudAmmo		Size		"132 36"	Deaccel	0.0		0.4
}

// ran if we just changed to a weapon that does not use clip ammo
event WeaponDoesNotUseClips
{
	Animate HudAmmo		Position	"r118 432"	Deaccel	0.0		0.4
	Animate HudAmmo		Size		"100 36"	Deaccel	0.0		0.4
}

event WeaponUsesSecondaryAmmo
{
	StopPanelAnimations	HudAmmoSecondary 	0.0
	Animate HudAmmoSecondary	FgColor		"0 0 0 0"	Linear	0.0		0.4
	Animate HudAmmoSecondary	BgColor		"0 0 0 0"	Linear	0.0		0.4
	Animate HudAmmoSecondary	Alpha		0		Linear	0.0		0.1
}

event WeaponDoesNotUseSecondaryAmmo
{
	StopPanelAnimations	HudAmmoSecondary 	0.0
	Animate HudAmmoSecondary	FgColor		"0 0 0 0"	Linear	0.0		0.4
	Animate HudAmmoSecondary	BgColor		"0 0 0 0"	Linear	0.0		0.4
	Animate HudAmmoSecondary	Alpha		0		Linear	0.0		0.1
}
