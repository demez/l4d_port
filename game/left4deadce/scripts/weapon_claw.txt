WeaponData
{
	"MaxPlayerSpeed"		"250"
	"WeaponType"			"claw"
	"WeaponPrice"			"0"
	"WeaponArmorRatio"		"1.7"
	"CrosshairMinDistance"		"7"
	"CrosshairDeltaDistance"	"3"
	"Team"				"Both" //"Zombie"
	"BuiltRightHanded"		"1"
	"PlayerAnimationExtension"	"knife"
	"MuzzleFlashScale"		"0"
	"MuzzleFlashStyle"		"CS_MUZZLEFLASH_NONE"
	
	"CanEquipWithShield"		"1"
	
	
	// Weapon characteristics:
	"Penetration"			"1"
	"Damage"			"50"//overwritten in code
	"Range"				"4096"//overwritten in code
	"RangeModifier"			"0.99"
	"Bullets"			"1"
	"CycleTime"			"1.0"
	
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"#Weapon_Claw"//overwritten in code
	"viewmodel"			"models/error.mdl"//overwritten in code
	
	"playermodel"			"models/shells/shell_556.mdl"
	
	"anim_prefix"			"anim"
	"bucket"			"6"
	"bucket_position"		"0"

	"clip_size"			"-1"
	"default_clip"			"1"
	"primary_ammo"			"None"
	"secondary_ammo"		"None"

	"weight"			"0"
	"item_flags"			"0"
	
	"refire_delay"		"0.8"//overwritten in code
	"damage_flags"		"4"
	"weapon_idle_time"	"0.5"

	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"reload"			"Default.Reload"
		"single_shot"			"Player.StopItem"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"DebugFixed"
				"character"	"A"
		}
		"weapon_s"
		{	
				"font"		"DebugFixed"
				"character"	"A"
		}
		"ammo"
		{
				"font"		"DebugFixed"
				"character"	"A"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
	ModelBounds
	{
		Viewmodel
		{
			Mins	"-2 -16 -15"
			Maxs	"18 11 5"
		}
		World
		{
			Mins	"-2 -5 -5"
			Maxs	"10 4 11"
		}
	}
}