credits I guess

Pistol model http://steamcommunity.com/sharedfiles/filedetails/?id=533595555
Lt. Rocky: Beretta port, animations and compile
Tripwire: Original model/textures/animations/audio

Keith's model https://steamcommunity.com/sharedfiles/filedetails/?id=183702118
Bloocobalt: Model
Treyarch: Keith's head
Karimatrix: Texturing
ZeqMacaw: Fixes with Melee Weapons, online Animations of Nick
K1CHWA: Fixed Hairmap to have an Alpha Layer
Reflectent, Sliferz and Dean: Misc

Brair's model https://steamcommunity.com/sharedfiles/filedetails/?id=776807043
Valve: Models/textures
Infinity ward: gasmask model/textures
twilight sparkle: csgo fp arms port
Professor heavy: csgo players port
Rex: rig and compile

UMP model http://steamcommunity.com/sharedfiles/filedetails/?id=903948955
Yafeu fula: Porting Model, Animations, Textures
Activision: Model/Animations
