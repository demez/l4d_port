#include "cbase.h"
#include "NextBot.h"
#include "tier0/memdbgon.h"

IMPLEMENT_CLIENTCLASS(C_NextBotCombatCharacter, DT_NextBot, NextBotCombatCharacter);

BEGIN_RECV_TABLE(C_NextBotCombatCharacter, DT_NextBot)
END_RECV_TABLE();

C_NextBotCombatCharacter::C_NextBotCombatCharacter()
	: BaseClass()
{
	
}

C_NextBotCombatCharacter::~C_NextBotCombatCharacter()
{

}