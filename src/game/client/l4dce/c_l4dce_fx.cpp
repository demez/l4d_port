#include "cbase.h"
#include "precache_register.h"
#include "iefx.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

PRECACHE_REGISTER_BEGIN( GLOBAL, PrecacheASW )
PRECACHE( PARTICLE_SYSTEM, "impact_steam" )
PRECACHE( PARTICLE_SYSTEM, "impact_steam_small" )
PRECACHE( PARTICLE_SYSTEM, "impact_steam_short" )
PRECACHE_REGISTER_END()

const char *s_pszBurstPipeEffects[]=
{
	"impact_steam",
	"impact_steam_small",
	"impact_steam_short"
};

ConVar asw_burst_pipe_chance( "asw_burst_pipe_chance", "0.25f" );

void FX_ASW_Potential_Burst_Pipe( const Vector &vecImpactPoint, const Vector &vecReflect, const Vector &vecShotBackward, const Vector &vecNormal )
{
	if ( RandomFloat() > asw_burst_pipe_chance.GetFloat() )
		return;

	const char *szEffectName = s_pszBurstPipeEffects[ RandomInt( 0, NELEMS( s_pszBurstPipeEffects) - 1 ) ];
	CUtlReference<CNewParticleEffect> pSteamEffect = CNewParticleEffect::CreateOrAggregate( NULL, szEffectName, vecImpactPoint, NULL );
	if ( pSteamEffect )
	{
		Vector vecImpactY, vecImpactZ;
		VectorVectors( vecNormal, vecImpactY, vecImpactZ ); 
		vecImpactY *= -1.0f;

		pSteamEffect->SetControlPoint( 0, vecImpactPoint );
		pSteamEffect->SetControlPointOrientation( 0, vecImpactZ, vecImpactY, vecNormal );
	}
}
