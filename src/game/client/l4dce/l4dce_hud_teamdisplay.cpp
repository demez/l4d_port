#include "cbase.h"
#include "hudelement.h"
#include "hud_macros.h"
#include "iclientmode.h"
#include <vgui_controls/controls.h>
#include <vgui_controls/Panel.h>
#include <vgui_controls/EditablePanel.h>
#include <vgui_controls/ImagePanel.h>
#include <vgui_controls/Image.h>
#include <vgui_controls/Label.h>
#include <vgui/ISurface.h>
#include "l4dce_player.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

using namespace vgui;

//MUST MATCH TerrorSurvivors!!!
static const char *const g_ppszPlayerHeadsIcons[NUM_SURVIVORS] =
{
	"", //SURVIVOR_INVALID

	"s_panel_coach.vtf", //SURVIVOR_COACH
	"s_panel_gambler.vtf", //SURVIVOR_NICK
	"s_panel_mechanic.vtf", //SURVIVOR_ELLIS
	"s_panel_producer.vtf", //SURVIVOR_ROCHELLE

	"s_panel_teenangst.vtf", //SURVIVOR_ZOEY
	"s_panel_namvet.vtf", //SURVIVOR_BILL
	"s_panel_biker.vtf", //SURVIVOR_FRANCIS
	"s_panel_manager.vtf", //SURVIVOR_LOUIS

	"s_panel_crazy.vtf", //SURVIVOR_KEITH
	"s_panel_pilot.vtf", //SURVIVOR_MICHAEL
	"s_panel_gasmask.vtf", //SURVIVOR_BRAIR
	"s_panel_fallen.vtf", //SURVIVOR_WHITAKER
};

class CHudTeamDisplay : public CHudElement, public EditablePanel
{
	DECLARE_CLASS_SIMPLE( CHudTeamDisplay, EditablePanel );
public:
	CHudTeamDisplay( const char *pElementName );
	virtual void	ApplySchemeSettings( IScheme *scheme );
};

DECLARE_HUDELEMENT( CHudTeamDisplay );

CHudTeamDisplay::CHudTeamDisplay( const char *pElementName ) :
	CHudElement( pElementName ), BaseClass( NULL, "CHudTeamDisplay" )
{
	vgui::Panel *pParent = GetClientMode()->GetViewport();
	SetParent( pParent );
}

void CHudTeamDisplay::ApplySchemeSettings( IScheme *scheme )
{
	BaseClass::ApplySchemeSettings( scheme );
	LoadControlSettings("Resource/UI/HUD/TeamDisplayHud.res");
}

enum SurvivorState
{
	STATE_DEAD,
	STATE_ALIVE,
	STATE_INCAPACITED
};

class HealthPanel : public EditablePanel
{
	DECLARE_CLASS_SIMPLE( HealthPanel, EditablePanel );
public:
	HealthPanel( Panel *parent, const char *pElementName );
	virtual void	ApplySchemeSettings( IScheme *scheme );
	virtual void	PaintBackground();
	void	GetTextureId( const char* texture, int &id );
	void	SetHealth( int health ) { m_nHealth = health; }
private:
	int m_nHealthBarHighId;
	int m_nHealthBarMediumId;
	int m_nHealthBarLowId;
	int m_nHealth;
};

HealthPanel::HealthPanel( Panel *parent, const char *pElementName ) : BaseClass( parent, "HealthPanel" )
{
	m_nHealthBarHighId = -1;
	m_nHealthBarMediumId = 1;
	m_nHealthBarLowId = 1;
	m_nHealth = 0;
}

void HealthPanel::ApplySchemeSettings(IScheme *scheme)
{
	BaseClass::ApplySchemeSettings(scheme);
	GetTextureId("vgui/hud/healthbar_withglow_green", m_nHealthBarHighId);
	GetTextureId("vgui/hud/healthbar_withglow_orange", m_nHealthBarMediumId);
	GetTextureId("vgui/hud/healthbar_withglow_red", m_nHealthBarLowId);
}

void HealthPanel::GetTextureId( const char* texture, int &id )
{
	id = vgui::surface()->DrawGetTextureId( texture );
	if ( id == -1 )
	{
		id = vgui::surface()->CreateNewTextureID();
		vgui::surface()->DrawSetTextureFile(id, texture, true, false);
	}
}

void HealthPanel::PaintBackground()
{
	float Health = ( float(m_nHealth) / 100.0f );
	Health = clamp(Health, 0, 1.0f);
	
	if ( m_nHealth < 25 )
		surface()->DrawSetTexture(m_nHealthBarLowId);
	else if ( m_nHealth < 40 )
		surface()->DrawSetTexture(m_nHealthBarMediumId);
	else
		surface()->DrawSetTexture(m_nHealthBarHighId);

	int Wide = Health * GetWide();
	float flUsedFrac = (float)Wide / (float)GetWide();
	// Textured bar
	surface()->DrawSetColor(Color(255, 255, 255, 255));

	DrawTexturedRectParms_t params;
	params.x0 = 0;
	params.y0 = 0;
	params.x1 = Wide;
	params.y1 = GetTall();
	params.s0 = 0;
	params.s1 = flUsedFrac;
	surface()->DrawTexturedRectEx( &params );	
}

DECLARE_BUILD_FACTORY( HealthPanel )

class TeamDisplayPlayer: public CHudElement, public EditablePanel
{
	DECLARE_CLASS_SIMPLE( TeamDisplayPlayer, EditablePanel );
public:
	TeamDisplayPlayer( Panel *parent, const char *pElementName );
	virtual void	ApplySchemeSettings( IScheme *scheme );
	virtual void	OnThink();

	ImagePanel *m_pHeadImage;
	ImagePanel *m_pDeadImage;
	ImagePanel *m_pDuckingIcon;
	Label	*m_pHealthIcon;
	Label	*m_pHealthNumber;
	HealthPanel	*m_pHealthPanel;

	C_TerrorPlayer *m_pPlayer;
	bool	m_bIsLocalPlayer;
	int		m_nState;
};

TeamDisplayPlayer::TeamDisplayPlayer( Panel *parent, const char *pElementName ) :
	CHudElement( pElementName ), BaseClass( parent, "TeammatePanel" )
{
}

void TeamDisplayPlayer::ApplySchemeSettings(IScheme *scheme)
{
	BaseClass::ApplySchemeSettings(scheme);

	if (m_bIsLocalPlayer)
		LoadControlSettings("Resource/UI/HUD/LocalPlayerPanel.res");
	else
		LoadControlSettings("Resource/UI/HUD/TeammatePanel.res");
	
	m_pHeadImage = dynamic_cast<ImagePanel *>( FindChildByName( "Head" ) );
	m_pDeadImage = dynamic_cast<ImagePanel *>( FindChildByName( "Dead" ) );
	m_pDuckingIcon = dynamic_cast<ImagePanel *>( FindChildByName( "DuckingIcon" ) );
	m_pHealthIcon = dynamic_cast<Label *>( FindChildByName( "HealthIcon" ) );
	m_pHealthNumber = dynamic_cast<Label *>( FindChildByName( "HealthNumber" ) );
	m_pHealthPanel = dynamic_cast<HealthPanel *>( FindChildByName( "Health" ) );
}

void TeamDisplayPlayer::OnThink()
{
	if ( m_bIsLocalPlayer )
	{
		m_pPlayer = ToTerrorPlayer(CBasePlayer::GetLocalPlayer());
	}

	if (!m_pPlayer)
		return;

	//Player just died
	if ( m_nState == STATE_ALIVE && !m_pPlayer->IsAlive() )
	{
		m_nState = STATE_DEAD;
		//Lets update this
		m_pDeadImage->SetVisible( true );
	}
	//Player just spawned
	else if ( m_nState == STATE_DEAD && m_pPlayer->IsAlive() )
	{
		m_nState = STATE_ALIVE;
		//Lets update this
		m_pDeadImage->SetVisible( false );
		m_pHeadImage->SetImage(g_ppszPlayerHeadsIcons[m_pPlayer->m_nSurvivor]);
	}
	if ( m_pDuckingIcon )
	{
		if ( !m_pDuckingIcon->IsVisible() && ( m_pPlayer->m_Local.m_bDucking || m_pPlayer->m_Local.m_bDucked ) )
		{
			m_pDuckingIcon->SetVisible(true);
		}
		else if ( m_pDuckingIcon->IsVisible() && !( m_pPlayer->m_Local.m_bDucking || m_pPlayer->m_Local.m_bDucked ) )
		{
			m_pDuckingIcon->SetVisible(false);
		}
	}
	if ( m_pHealthIcon )
	{
		if ( m_pPlayer->GetHealth() < 25 )
			m_pHealthIcon->SetFgColor(Color(165, 25, 30, 255));
		else if ( m_pPlayer->GetHealth() < 40 )
			m_pHealthIcon->SetFgColor(Color(215, 145, 10, 255));
		else
			m_pHealthIcon->SetFgColor(Color(15, 180, 50, 255));
	
	}
	if ( m_pHealthNumber )
	{
		if ( m_pPlayer->GetHealth() < 25 )
			m_pHealthNumber->SetFgColor(Color(165, 25, 30, 255));
		else if ( m_pPlayer->GetHealth() < 40 )
			m_pHealthNumber->SetFgColor(Color(215, 145, 10, 255));
		else
			m_pHealthNumber->SetFgColor(Color(15, 180, 50, 255));
	}
	if ( m_pHealthPanel )
		m_pHealthPanel->SetHealth(m_pPlayer->GetHealth());

	SetDialogVariable("HealthNumber", m_pPlayer->GetHealth());
}

DECLARE_BUILD_FACTORY( TeamDisplayPlayer )
DECLARE_BUILD_FACTORY_CUSTOM_ALIAS(TeamDisplayPlayer, LocalPlayerPanel, Create_TeamDisplayPlayer )

class CHudLocalPlayerDisplay: public CHudElement, public EditablePanel
{
	DECLARE_CLASS_SIMPLE( CHudLocalPlayerDisplay, EditablePanel );
public:
	CHudLocalPlayerDisplay( const char *pElementName );
	virtual void	ApplySchemeSettings( IScheme *scheme );
};

DECLARE_HUDELEMENT( CHudLocalPlayerDisplay );

CHudLocalPlayerDisplay::CHudLocalPlayerDisplay( const char *pElementName ) :
	CHudElement( pElementName ), BaseClass( NULL, "CHudLocalPlayerDisplay" )
{
	vgui::Panel *pParent = GetClientMode()->GetViewport();
	SetParent( pParent );
}

void CHudLocalPlayerDisplay::ApplySchemeSettings( IScheme *scheme )
{
	BaseClass::ApplySchemeSettings( scheme );
	LoadControlSettings("Resource/UI/HUD/LocalPlayerDisplay.res");

	TeamDisplayPlayer *pLocalPlayerPanel = (TeamDisplayPlayer*)FindChildByName("LocalPlayer");
	if ( pLocalPlayerPanel )
		pLocalPlayerPanel->m_bIsLocalPlayer = true;
}