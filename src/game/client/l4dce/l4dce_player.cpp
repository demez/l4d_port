#include "cbase.h"
#include "l4dce_player.h"
#include "l4dce_playeranimstate.h"
#include "c_basetempentity.h"
#include "prediction.h"
#include "weapon_terror_base.h"
#include "tier0/memdbgon.h"

#ifdef CTerrorPlayer
#undef CTerrorPlayer
#endif

static ConVar cl_prefered_survivor("cl_prefered_survivor", "6", FCVAR_USERINFO|FCVAR_ARCHIVE,
	"Selected survivor.\n1 to 9. Coach Nick Ellis Rochelle Zoey Bill Francis Louis Keith\n",
	true, FIRST_SURVIVOR, true, LAST_SURVIVOR);

class C_TEPlayerAnimEvent : public C_BaseTempEntity
{
	public:
		DECLARE_CLASS(C_TEPlayerAnimEvent, C_BaseTempEntity);
		DECLARE_CLIENTCLASS();

		virtual void PostDataUpdate(DataUpdateType_t updateType)
		{
			VPROF("C_TEPlayerAnimEvent::PostDataUpdate");

			if(m_iPlayerIndex == -1)
				return;

			EHANDLE hPlayer = cl_entitylist->GetNetworkableHandle(m_iPlayerIndex);
			if(!hPlayer)
				return;

			C_TerrorPlayer *pPlayer = dynamic_cast<C_TerrorPlayer *>(hPlayer.Get());
			if(pPlayer && !pPlayer->IsDormant())
				pPlayer->DoAnimationEvent((PlayerAnimEvent_t)m_iEvent.Get(), m_nData);
		}

	public:
		CNetworkVar(int, m_iPlayerIndex);
		CNetworkVar(int, m_iEvent);
		CNetworkVar(int, m_nData);
};

IMPLEMENT_CLIENTCLASS_EVENT(C_TEPlayerAnimEvent, DT_TEPlayerAnimEvent, CTEPlayerAnimEvent);

BEGIN_RECV_TABLE_NOBASE(C_TEPlayerAnimEvent, DT_TEPlayerAnimEvent)
	RecvPropInt(RECVINFO(m_iPlayerIndex)),
	RecvPropInt(RECVINFO(m_iEvent)),
	RecvPropInt(RECVINFO(m_nData))
END_RECV_TABLE();

LINK_ENTITY_TO_CLASS(player, C_TerrorPlayer);
PRECACHE_REGISTER(player);

IMPLEMENT_CLIENTCLASS(C_TerrorPlayer, DT_TerrorPlayer, CTerrorPlayer);

BEGIN_RECV_TABLE_NOBASE(C_TerrorPlayer, DT_TerrorLocalPlayerExclusive)
	RecvPropVector(RECVINFO_NAME(m_vecNetworkOrigin, m_vecOrigin)),
	RecvPropFloat(RECVINFO(m_angEyeAngles[0])),
END_RECV_TABLE()

BEGIN_RECV_TABLE_NOBASE(C_TerrorPlayer, DT_TerrorNonLocalPlayerExclusive)
	RecvPropVector(RECVINFO_NAME(m_vecNetworkOrigin, m_vecOrigin)),
	RecvPropFloat(RECVINFO(m_angEyeAngles[0])),
	RecvPropFloat(RECVINFO(m_angEyeAngles[1])),
END_RECV_TABLE()

BEGIN_RECV_TABLE(C_TerrorPlayer, DT_TerrorPlayer)
	RecvPropDataTable("terror_localdata", 0, 0, &REFERENCE_RECV_TABLE(DT_TerrorLocalPlayerExclusive)),
	RecvPropDataTable("terror_nonlocaldata", 0, 0, &REFERENCE_RECV_TABLE(DT_TerrorNonLocalPlayerExclusive)),
	RecvPropInt(RECVINFO(m_nSurvivor)),
END_RECV_TABLE();

BEGIN_PREDICTION_DATA(C_TerrorPlayer)
	DEFINE_PRED_FIELD(m_flCycle, FIELD_FLOAT, FTYPEDESC_OVERRIDE|FTYPEDESC_PRIVATE|FTYPEDESC_NOERRORCHECK),
	DEFINE_PRED_FIELD(m_nSequence, FIELD_INTEGER, FTYPEDESC_OVERRIDE|FTYPEDESC_PRIVATE|FTYPEDESC_NOERRORCHECK),
	DEFINE_PRED_FIELD(m_nNewSequenceParity, FIELD_INTEGER, FTYPEDESC_OVERRIDE|FTYPEDESC_PRIVATE|FTYPEDESC_NOERRORCHECK),
	DEFINE_PRED_FIELD(m_nResetEventsParity, FIELD_INTEGER, FTYPEDESC_OVERRIDE|FTYPEDESC_PRIVATE|FTYPEDESC_NOERRORCHECK),
END_PREDICTION_DATA();

C_TerrorPlayer::C_TerrorPlayer()
	: BaseClass(), m_iv_angEyeAngles("C_TerrorPlayer::m_iv_angEyeAngles")
{
	SetPredictionEligible(true);

	m_angEyeAngles.Init();

	AddVar(&m_angEyeAngles, &m_iv_angEyeAngles, LATCH_SIMULATION_VAR);
	AddVar(&m_Local.m_vecPunchAngle, &m_Local.m_iv_vecPunchAngle, LATCH_SIMULATION_VAR);
	AddVar(&m_Local.m_vecPunchAngleVel, &m_Local.m_iv_vecPunchAngleVel, LATCH_SIMULATION_VAR);

	static ConVarRef r_flashlightscissor("r_flashlightscissor");
	r_flashlightscissor.SetValue(false);

	#ifdef SWARM_DLL
	engine->ClientCmd("gameui_hide");
	#endif

	m_pAnimState = CreateTerrorPlayerAnimState(this);
}

C_TerrorPlayer::~C_TerrorPlayer()
{
	if(m_pAnimState) {
		m_pAnimState->ClearAnimationState();
		delete m_pAnimState;
	}
	m_pAnimState = nullptr;
}

void C_TerrorPlayer::PostDataUpdate(DataUpdateType_t updateType)
{
	SetNetworkAngles(GetLocalAngles());
	
	BaseClass::PostDataUpdate(updateType);
}

void C_TerrorPlayer::OnDataChanged(DataUpdateType_t type)
{
	BaseClass::OnDataChanged(type);

	UpdateVisibility();
}

bool C_TerrorPlayer::ShouldRegenerateOriginFromCellBits() const
{
	return true;
}

const QAngle &C_TerrorPlayer::EyeAngles()
{
	if(IsLocalPlayer())
		return BaseClass::EyeAngles();
	else
		return m_angEyeAngles;
}

const QAngle &C_TerrorPlayer::GetRenderAngles()
{
	if(IsRagdoll())
		return vec3_angle;
	else
		return m_pAnimState->GetRenderAngles();
}

void C_TerrorPlayer::SetSurvivor(TerrorSurvivors survivor)
{
	
}

TerrorSurvivors C_TerrorPlayer::GetSurvivor() const
{
	return m_nSurvivor;
}

void C_TerrorPlayer::SetAnimation(PLAYER_ANIM playerAnim)
{
	switch(playerAnim)
	{
		case PLAYER_IDLE: {  break; }
		case PLAYER_WALK: {  break; }
		case PLAYER_JUMP: { DoAnimationEvent(PLAYERANIMEVENT_JUMP); break; }
		case PLAYER_SUPERJUMP: {  break; }
		case PLAYER_DIE: { DoAnimationEvent(PLAYERANIMEVENT_DIE); break; }
		case PLAYER_ATTACK1: { DoAnimationEvent(PLAYERANIMEVENT_ATTACK_PRIMARY); break; }
		case PLAYER_IN_VEHICLE: {  break; }
		case PLAYER_RELOAD: { DoAnimationEvent(PLAYERANIMEVENT_RELOAD); break; }
		case PLAYER_START_AIMING: {  break; }
		case PLAYER_LEAVE_AIMING: {  break; }
	}
}

void C_TerrorPlayer::DoAnimationEvent(PlayerAnimEvent_t event, int nData)
{
	if(IsLocalPlayer()) {
		if(!prediction->IsFirstTimePredicted())
			return;
	}

	MDLCACHE_CRITICAL_SECTION();
	m_pAnimState->DoAnimationEvent(event, nData);
}

CStudioHdr *C_TerrorPlayer::OnNewModel()
{
	CStudioHdr *hdr = BaseClass::OnNewModel();
	m_pAnimState->OnNewModel();
	return hdr;
}

void C_TerrorPlayer::UpdateClientSideAnimation()
{
	BaseClass::UpdateClientSideAnimation();

	if(IsLocalPlayer())
		m_pAnimState->Update(EyeAngles()[YAW], EyeAngles()[PITCH]);
	else
		m_pAnimState->Update(m_angEyeAngles[YAW], m_angEyeAngles[PITCH]);
}

C_TerrorPlayer *C_TerrorPlayer::GetLocalPlayer(int nSlot)
{
	return static_cast<C_TerrorPlayer *>(BaseClass::GetLocalPlayer(nSlot));
}

C_TerrorPlayer *ToTerrorPlayer(C_BaseEntity *pEntity)
{
	if(!pEntity || !pEntity->IsPlayer())
		return nullptr;

#ifdef _DEBUG
	Assert(dynamic_cast<C_TerrorPlayer *>(pEntity) != nullptr);
#endif

	return static_cast<C_TerrorPlayer *>(pEntity);
}

C_TerrorPlayer *C_TerrorPlayer::GetLocalTerrorPlayer()
{
	return static_cast<C_TerrorPlayer *>(C_BasePlayer::GetLocalPlayer());
}

CTerrorWeaponBase *C_TerrorPlayer::GetActiveTerrorWeapon(void) const
{
	CBaseCombatWeapon *pRet = GetActiveWeapon();

	#ifdef _DEBUG
	Assert(dynamic_cast<CTerrorWeaponBase *>(pRet) != nullptr);
	#endif

	return static_cast<CTerrorWeaponBase *>(pRet);
}

bool C_TerrorPlayer::IsWeaponLowered(void)
{
	return false;
}

/*
extern ConVar r_flashlightfar;
extern ConVar r_flashlightfov;
extern ConVar r_flashlightlinear;

const char *C_TerrorPlayer::GetFlashlightTextureName() const
{
	return "effects/flashlight001";
}

float C_TerrorPlayer::GetFlashlightFOV() const
{
	return r_flashlightfov.GetFloat();
}

float C_TerrorPlayer::GetFlashlightFarZ() const
{
	return r_flashlightfar.GetFloat();
}

float C_TerrorPlayer::GetFlashlightLinearAtten() const
{
	return r_flashlightlinear.GetFloat();
}

bool C_TerrorPlayer::CastsFlashlightShadows() const
{
	return true;
}
*/