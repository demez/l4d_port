#include "cbase.h"
#include "l4dce_playerselect.h"
#include "vmainmenu.h"
#include "BaseModPanel.h"
#include "vgui_controls/ImagePanel.h"
#include "animation.h"
#include "iclientmode.h"
#include "tier0/memdbgon.h"

static CPlayerSelect *g_pPlayerSelect = nullptr;

CON_COMMAND(makeplayer, "")
{
	if(g_pPlayerSelect)
		delete g_pPlayerSelect;
	g_pPlayerSelect = nullptr;

	g_pPlayerSelect = new CPlayerSelect();
}

CPlayerModelViewer::CPlayerModelViewer(vgui::Panel *pParent, TerrorSurvivors survivor)
	: BaseClass(pParent, "CPlayerModelViewer"), m_nSurvivor(survivor)
{
	SetZPos(pParent->GetZPos()+1);

	SetKeyBoardInputEnabled(false);
	SetMouseInputEnabled(false);

	SetCameraFOV(90.0f);

	static const Color clr(63, 63, 63, 255);
	SetBackgroundColor(clr);

	const char *const model = GetModelForSurvivor(survivor);
	SetName(model);

	SetMDL(model);
	SetModelAnim("ACT_IDLE_CALM_PISTOL");
}

const QAngle &CPlayerModelViewer::GetCamAngOffset()
{
	Assert((m_nSurvivor < NUM_SURVIVORS) && (m_nSurvivor > SURVIVOR_INVALID));

	static const QAngle default(0.0f, 0.0f, 0.0f);

	//MUST MATCH TerrorSurvivors!!!
	static const QAngle array[NUM_SURVIVORS] = 
	{
		vec3_angle, //SURVIVOR_INVALID

		default, //SURVIVOR_COACH
		default, //SURVIVOR_NICK
		default, //SURVIVOR_ELLIS
		default, //SURVIVOR_ROCHELLE

		default, //SURVIVOR_ZOEY
		default, //SURVIVOR_BILL
		default, //SURVIVOR_FRANCIS
		default, //SURVIVOR_LOUIS

		default, //SURVIVOR_KEITH
		default, //SURVIVOR_MICHAEL
		default, //SURVIVOR_BRAIR
		default, //SURVIVOR_WHITAKER
	};
	static const int arraysize = ARRAYSIZE(array);

	if(m_nSurvivor >= arraysize)
		return default;

	return array[m_nSurvivor];
}

const Vector &CPlayerModelViewer::GetCamPosOffset()
{
	Assert((m_nSurvivor < NUM_SURVIVORS) && (m_nSurvivor > SURVIVOR_INVALID));

	static const Vector default(0.0f, 0.0f, 0.0f);

	//MUST MATCH TerrorSurvivors!!!
	static const Vector array[NUM_SURVIVORS] = 
	{
		vec3_invalid, //SURVIVOR_INVALID

		Vector(0.0f, 0.0f, -8.0f), //SURVIVOR_COACH
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_NICK
		Vector(0.0f, 0.0f, -8.0f), //SURVIVOR_ELLIS
		Vector(0.0f, 0.0f, -15.0f), //SURVIVOR_ROCHELLE

		Vector(0.0f, 0.0f, -15.0f), //SURVIVOR_ZOEY
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_BILL
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_FRANCIS
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_LOUIS

		Vector(0.0f, 0.0f, -8.0f), //SURVIVOR_KEITH
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_MICHAEL
		Vector(0.0f, 0.0f, -6.0f), //SURVIVOR_BRAIR
		default, //SURVIVOR_WHITAKER //fix this when we have their model
	};
	static const int arraysize = ARRAYSIZE(array);

	if(m_nSurvivor >= arraysize)
		return default;

	return array[m_nSurvivor];
}

void callback(IConVar *var, const char *pOldValue, float flOldValue)
{
	g_pPlayerSelect->InvalidateLayout();
}

static ConVar test1("test1", "0", FCVAR_NONE, "", callback);

void CPlayerModelViewer::PerformLayout()
{
	BaseClass::PerformLayout();

	Vector min; Vector max;
	GetBoundingBox(min, max);

	Vector origin(max.x, 0.0f, max.z);
	QAngle angles(0.0f, -180.0f, 0.0f);

	origin += GetCamPosOffset();
	angles += GetCamAngOffset();

	origin.z += test1.GetFloat();

	SetCameraPositionAndAngles(origin, angles);
}

CPlayerModelViewer::~CPlayerModelViewer()
{
	
}

CPlayerSelect::CPlayerSelect()
	: BaseClass(nullptr, "PlayerSelect")
{
	BaseModUI::MainMenu *pMainMenu = static_cast<BaseModUI::MainMenu *>(BaseModUI::CBaseModPanel::GetSingleton().GetWindow(BaseModUI::WT_MAINMENU));
	SetParent(pMainMenu);
	//SetParent(GetClientMode()->GetViewport());

	int w; int h;
	GetHudSize(w, h);
	SetBounds(0, 0, w, h);
	SetZPos(GetZPos()+1);

	m_pBackground = new vgui::ImagePanel(this, "Background");

	bool bIsWidescreen = (((float)w/(float)h) >= 1.5999f);

	if(bIsWidescreen)
		m_pBackground->SetImage("../console/background01_widescreen");
	else
		m_pBackground->SetImage("../console/background01");

	m_pBackground->SetShouldScaleImage(true);

	m_pScreenText = new vgui::Label(this, "ScreenText", "Model Select");

	for(int i = FIRST_SURVIVOR; i <= LAST_SURVIVOR; i++)
	{
		CPlayerModelViewer *panel = new CPlayerModelViewer(this, (TerrorSurvivors)i);
		m_ModelsPanelList.AddToTail(panel);
	}
}

CPlayerSelect::~CPlayerSelect()
{
	m_ModelsPanelList.PurgeAndDeleteElements();

	if(m_pScreenText)
		delete m_pScreenText;
	m_pScreenText = nullptr;

	if(m_pBackground)
		delete m_pBackground;
	m_pBackground = nullptr;
}

void CPlayerSelect::PerformLayout()
{
	BaseClass::PerformLayout();

	int x; int y; int w; int h;
	GetBounds(x, y, w, h);
	int z = GetZPos();

	if(m_pBackground) {
		m_pBackground->SetBounds(0, 0, w, h);
		m_pBackground->SetZPos(z-1);
	}

	if(m_pScreenText) {
		int t = h/20;
		m_pScreenText->SetBounds(0, 0, w/10, t);
		m_pScreenText->SetZPos(z+1);
		y += t;
	}

	x = 0;
	y = (h/20);
	int c = clamp(m_ModelsPanelList.Count(), 0, 4);
	FOR_EACH_VEC(m_ModelsPanelList, i)
	{
		int ws = (w/c);
		int hs = (h/c);

		if(x+ws > w)
		{
			y += hs;
			x = 0;
			i--;
			continue;
		}

		int pad = 2;

		CPlayerModelViewer *&it = m_ModelsPanelList.Element(i);
		it->SetBounds(x, y, ws-pad, hs-pad);
		it->SetZPos(z+1);
		x += ws;
	}
}

void CPlayerSelect::InvalidateLayout(bool layoutNow, bool reloadScheme)
{
	BaseClass::InvalidateLayout(layoutNow, reloadScheme);

	FOR_EACH_VEC(m_ModelsPanelList, i)
	{
		CPlayerModelViewer *&it = m_ModelsPanelList.Element(i);
		it->InvalidateLayout(layoutNow, reloadScheme);
	}
}
