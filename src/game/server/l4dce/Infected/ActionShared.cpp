#include "cbase.h"
#include "nav_area.h"
#include "Infected.h"
#include "ActionShared.h"
#include "ActionWander.h"
#include "ActionAttack.h"
#include "l4dce_shareddefs.h"
#include "tier0/memdbgon.h"

InfectedPathCost::InfectedPathCost()
{
	m_iEntindex = 1;
	m_iTeamNum = TEAM_INFECTED;
}

InfectedPathCost::InfectedPathCost(Infected *infected)
{
	SetInfected(infected);
}

void InfectedPathCost::SetInfected(Infected *infected)
{
	m_pInfected = infected;
	m_pLocomotion = m_pInfected->GetLocomotionInterface();
	m_flStepHeight = m_pLocomotion->GetStepHeight();
	m_flMaxJumpHeight = m_pLocomotion->GetMaxJumpHeight();
	m_flDeathDropHeight = m_pLocomotion->GetDeathDropHeight();
	m_iEntindex = m_pInfected->entindex();
	m_iTeamNum = m_pInfected->GetTeamNumber();
}

float InfectedPathCost::operator()(CNavArea *area, CNavArea *fromArea, const CNavLadder *ladder, const CFuncElevator *elevator, float length) const
{
	if(!fromArea)
		return 0.0f;

	if(m_pLocomotion && !m_pLocomotion->IsAreaTraversable(area))
		return -1.0f;

	float dist = 0.0f;

	if(ladder)
		dist = 2.0f * ladder->m_length;
	else if(length > 0.0f)
		dist = length;
	else
		dist = (area->GetCenter() - fromArea->GetCenter()).Length();

	float cost = dist + fromArea->GetCostSoFar();

	float deltaZ = fromArea->ComputeAdjacentConnectionHeightChange(area);
	if(deltaZ >= m_flStepHeight) {
		if(deltaZ >= m_flMaxJumpHeight)
			return -1.0f;
		cost += 5.0f * dist;
	} else if(deltaZ < -m_flDeathDropHeight)
		return -1.0f;

	float multiplier = RandomFloat(1.0f, 5.0f);

	int seed = (int)(gpGlobals->curtime * 0.1f) + 1;
	seed *= area->GetID();
	seed *= m_iEntindex;
	multiplier += (FastCos((float)seed) + 1.0f) * 50.0f;

	cost *= multiplier;

	return cost;
}

Action<Infected> *InfectedExecAction::InitialContainedAction(Infected *me)
{
	return new InfectedBehavior();
}

EventDesiredResult<Infected> InfectedExecAction::OnInjured(Infected *me, const CTakeDamageInfo &info)
{
	me->AddGesture(ACT_TERROR_FLINCH);
	return TryContinue();
}

EventDesiredResult<Infected> InfectedBehavior::OnKilled(Infected *me, const CTakeDamageInfo &info)
{
	return TryChangeTo(new InfectedDying(info), RESULT_CRITICAL);
}

#ifdef DEBUG
class InfectedDebugAction : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedDebugAction"; }

		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override
		{
			return Continue();
		}

		virtual ActionResult<Infected> Update(Infected *me, float interval) override
		{
			return Continue();
		}

	private:
		
};
#endif

Action<Infected> *InfectedBehavior::InitialContainedAction(Infected *me)
{
	//return new InfectedDebugAction();
	return new InfectedWander();
}

InfectedChangePosture::InfectedChangePosture(IBody::PostureType desired)
	: Action<Infected>(), m_nDesiredPosture(desired)
{

}

ActionResult<Infected> InfectedChangePosture::OnStart(Infected *me, Action<Infected> *priorAction)
{
	me->GetBodyInterface()->SetDesiredPosture(m_nDesiredPosture);
	return Continue();
}

ActionResult<Infected> InfectedChangePosture::Update(Infected *me, float interval)
{
	IBody *body = me->GetBodyInterface();

	if(body->IsInDesiredPosture())
		return Done();
	
	if(!body->IsPostureChanging())
		body->SetDesiredPosture(m_nDesiredPosture);

	return Continue();
}

InfectedStandingActivity::InfectedStandingActivity(Activity activity, float time, Action<Infected> *action, float cycle)
	: Action<Infected>(), m_nActivity(activity), m_flTime(time), m_pAction(action), m_flCycle(cycle)
{
	
}

EventDesiredResult<Infected> InfectedStandingActivity::OnAnimationActivityComplete(Infected *me, int activity)
{
	if(activity == m_nActivity)
		m_bDone = true;

	return TryContinue();
}

ActionResult<Infected> InfectedStandingActivity::OnStart(Infected *me, Action<Infected> *priorAction)
{
	if(!me->GetBodyInterface()->IsPostureMobile())
		return SuspendFor(new InfectedChangePosture(IBody::STAND));

	OnResume(me, nullptr);
	return Continue();
}

ActionResult<Infected> InfectedStandingActivity::OnResume(Infected *me, Action<Infected> *interruptingAction)
{
	if(m_flTime != 0.0f)
		m_Timer.Start(m_flTime);

	if(!me->GetBodyInterface()->StartActivity(m_nActivity, IBody::MOTION_CONTROLLED_XY)) {
		m_bDone = true;
		return Continue();
	}

	if(m_flCycle != 0.0f)
		me->SetCycle(m_flCycle);

	return Continue();
}

ActionResult<Infected> InfectedStandingActivity::Update(Infected *me, float interval)
{
	if(m_Timer.HasStarted() && m_Timer.IsElapsed())
		m_bDone = true;

	if(m_bDone && !m_pAction)
		return Done();
	else if(m_bDone && m_pAction)
		return ChangeTo(m_pAction);

	return Continue();
}

InfectedDying::InfectedDying(const CTakeDamageInfo &info)
	: Action<Infected>(), m_DeathInfo(info)
{

}

ActionResult<Infected> InfectedDying::OnStart(Infected *me, Action<Infected> *priorAction)
{
	IBody *body = me->GetBodyInterface();
	ILocomotion *locomotion = me->GetLocomotionInterface();

	me->SetSolid(SOLID_NONE);

	TheNavMesh->IncreaseDangerNearby(TEAM_INFECTED, 1.0f, me->GetLastKnownArea(), me->GetPosition(), me->GetVisionInterface()->GetMaxVisionRange(), -1.0f);

	if(locomotion->IsClimbingOrJumping() || !locomotion->IsOnGround())
		m_nActvity = ACT_DIERAGDOLL;
	else if(!(m_DeathInfo.GetDamageType() & DMG_BUCKSHOT)) {
		if(locomotion->IsRunning())
			m_nActvity = ACT_TERROR_DIE_WHILE_RUNNING;
		else
			m_nActvity = ACT_TERROR_DIE_FROM_STAND;
	} else {
		switch(DirectionBetweenEntities(me, m_DeathInfo.GetAttacker())) {
			default:
			case FORWARD: m_nActvity = ACT_TERROR_DIE_BACKWARD_FROM_SHOTGUN; break;
			case BACKWARD: m_nActvity = ACT_TERROR_DIE_FORWARD_FROM_SHOTGUN; break;
			case RIGHT: m_nActvity = ACT_TERROR_DIE_LEFTWARD_FROM_SHOTGUN; break;
			case LEFT: m_nActvity = ACT_TERROR_DIE_RIGHTWARD_FROM_SHOTGUN; break;
		}
	}

	//body->Reset();
	if(!body->StartActivity(m_nActvity, IBody::ACTIVITY_UNINTERRUPTIBLE | IBody::MOTION_CONTROLLED_XY)) {
		OnAnimationActivityComplete(me, m_nActvity);
		return Done();
	}
	
	return Continue();
}

EventDesiredResult<Infected> InfectedDying::OnAnimationActivityComplete(Infected *me, int activity)
{
	if(activity == m_nActvity) {
		#pragma message(FILE_LINE_STRING " remove this hack below when i find out why shotguns set velocity on ragdoll")
		if(m_DeathInfo.GetDamageType() & DMG_BUCKSHOT)
			m_DeathInfo = CTakeDamageInfo();
		me->CBaseCombatCharacter::Event_Killed(m_DeathInfo);
		return TryDone();
	}

	return TryContinue();
}

NavRelativeDirType DirectionBetweenEntities(CBaseEntity *ent1, CBaseEntity *ent2)
{
	Vector diff = (ent1->GetAbsOrigin() - ent2->GetAbsOrigin());

	QAngle angles;
	VectorAngles(diff, angles);

	angles = TransformAnglesToLocalSpace(angles, ent1->EntityToWorldTransform());

	NavDirType dir = AngleToDirection(angles.y);

	switch(dir) {
		default:
		case NORTH: return LEFT;
		case EAST: return BACKWARD;
		case SOUTH: return RIGHT;
		case WEST: return FORWARD;
	}
}