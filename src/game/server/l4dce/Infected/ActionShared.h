#ifndef _ACTION_SHARED_H_
#define _ACTION_SHARED_H_

#pragma once

#include "Path/NextBotPath.h"

class IPathCost;
class CNavArea;
class CNavLadder;
class CFuncElevator;
class Infected;
class ILocomotion;

class InfectedPathCost : public IPathCost
{
	public:
		InfectedPathCost();
		InfectedPathCost(Infected *infected);

		void SetInfected(Infected *infected);

		virtual float operator()(CNavArea *area, CNavArea *fromArea, const CNavLadder *ladder, const CFuncElevator *elevator, float length) const override;

	private:
		Infected *m_pInfected = nullptr;
		ILocomotion *m_pLocomotion = nullptr;
		int m_iEntindex = -1;
		int m_iTeamNum = -1;
		float m_flStepHeight = 0.0f;
		float m_flMaxJumpHeight = 0.0f;
		float m_flDeathDropHeight = 0.0f;
};

class InfectedExecAction : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedExecAction"; }
		virtual Action<Infected> *InitialContainedAction(Infected *me) override;
		virtual EventDesiredResult<Infected> OnInjured(Infected *me, const CTakeDamageInfo &info) override;
};

class InfectedBehavior : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedBehavior"; }
		virtual Action<Infected> *InitialContainedAction(Infected *me) override;
		virtual EventDesiredResult<Infected> OnKilled(Infected *me, const CTakeDamageInfo &info) override;
};

class InfectedChangePosture : public Action<Infected>
{
	public:
		InfectedChangePosture(IBody::PostureType desired);
		virtual const char *GetName() const override { return "InfectedChangePosture"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;

	private:
		IBody::PostureType m_nDesiredPosture = IBody::STAND;
};

class InfectedStandingActivity : public Action<Infected>
{
	public:
		InfectedStandingActivity(Activity activity, float time=0.0f, Action<Infected> *action=nullptr, float cycle=0.0f);
		virtual const char *GetName() const override { return "InfectedStandingActivity"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;
		virtual EventDesiredResult<Infected> OnAnimationActivityComplete(Infected *me, int activity) override;
		virtual ActionResult<Infected> OnResume(Infected *me, Action<Infected> *interruptingAction) override;

	private:
		CountdownTimer m_Timer;
		Activity m_nActivity = ACT_INVALID;
		bool m_bDone = false;
		float m_flTime = 0.0f;
		float m_flCycle = 0.0f;
		Action<Infected> *m_pAction = nullptr;
};

class InfectedDying : public Action<Infected>
{
	public:
		InfectedDying(const CTakeDamageInfo &info);
		virtual const char *GetName() const override { return "InfectedDying"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual EventDesiredResult<Infected> OnAnimationActivityComplete(Infected *me, int activity) override;

	private:
		CTakeDamageInfo m_DeathInfo;
		Activity m_nActvity = ACT_INVALID;
};

NavRelativeDirType DirectionBetweenEntities(CBaseEntity *ent1, CBaseEntity *ent2);

#endif