#include "cbase.h"
#include "Infected.h"
#include "ActionWander.h"
#include "ActionAttack.h"
#include "l4dce_shareddefs.h"
#include "tier0/memdbgon.h"

Action<Infected> *InfectedWander::InitialContainedAction(Infected *me)
{
	return new InfectedStandDazed();
}

ActionResult<Infected> InfectedWander::Update(Infected *me, float interval)
{
	const CKnownEntity *threat = me->GetVisionInterface()->GetPrimaryKnownThreat();
	if(!threat)
		return Continue();

	me->GetBodyInterface()->AimHeadTowards(threat->GetEntity(), IBody::IMPORTANT, 1.0f, nullptr);

	return Continue();
}

ActionResult<Infected> InfectedWander::OnStart(Infected *me, Action<Infected> *priorAction)
{
	me->GetBodyInterface()->SetArousal(IBody::NEUTRAL);
	return Continue();
}

EventDesiredResult<Infected> InfectedWander::OnContact(Infected *me, CBaseEntity *other, CGameTrace *result)
{
	if(!other->IsPlayer() || other->GetTeamNumber() != TEAM_SURVIVOR)
		return TryContinue();

	return TryChangeTo(new InfectedAlert(other, InfectedAlert::CONTACT));
}

EventDesiredResult<Infected> InfectedWander::OnInjured(Infected *me, const CTakeDamageInfo &info)
{
	CBaseEntity *attacker = info.GetAttacker();
	if(!attacker)
		return TryContinue();

	if(!attacker->IsPlayer() || attacker->GetTeamNumber() != TEAM_SURVIVOR)
		return TryContinue();

	return TryChangeTo(new InfectedAlert(attacker, InfectedAlert::INJURED));
}

EventDesiredResult<Infected> InfectedWander::OnSight(Infected *me, CBaseEntity *subject)
{
	if(!subject->IsPlayer() || subject->GetTeamNumber() != TEAM_SURVIVOR)
		return TryContinue();

	return TryChangeTo(new InfectedAlert(subject, InfectedAlert::SIGHT));
}

ActionResult<Infected> InfectedStandDazed::OnStart(Infected *me, Action<Infected> *priorAction)
{
	if(!me->GetBodyInterface()->IsPostureMobile())
		return SuspendFor(new InfectedChangePosture(IBody::STAND));

	OnResume(me, nullptr);
	return Continue();
}

ActionResult<Infected> InfectedStandDazed::OnResume(Infected *me, Action<Infected> *interruptingAction)
{
	me->GetBodyInterface()->StartActivity(ACT_TERROR_IDLE_NEUTRAL, IBody::MOTION_CONTROLLED_XY);
	return Continue();
}

