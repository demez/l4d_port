#ifndef _ACTION_WANDER_H_
#define _ACTION_WANDER_H_

#pragma once

#include "NextBotBehavior.h"
#include "Path/NextBotPathFollow.h"
#include "ActionShared.h"

class Infected;
class InfectedPathCost;

class InfectedWander : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedWander"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual Action<Infected> *InitialContainedAction(Infected *me) override;
		virtual EventDesiredResult<Infected> OnSight(Infected *me, CBaseEntity *subject) override;
		virtual EventDesiredResult<Infected> OnInjured(Infected *me, const CTakeDamageInfo &info) override;
		virtual EventDesiredResult<Infected> OnContact(Infected *me, CBaseEntity *other, CGameTrace *result = nullptr) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;
};

class InfectedStandDazed : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedStandDazed"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> OnResume(Infected *me, Action<Infected> *interruptingAction) override;
};

#endif