#include "cbase.h"
#include "Director.h"
#include "nav_mesh.h"
#include "functorutils.h"
#include "l4dce_shareddefs.h"
#include "tier0/memdbgon.h"

Director director;
Director &TheDirector() { return director;  }

ConVar director_debug("director_debug", "0", FCVAR_CHEAT, "Displays director status on screen");
ConVar director_candidate_interval("director_candidate_interval", "1.0", FCVAR_CHEAT, "Interval between updating candidate spawning areas");
ConVar director_horde_min_distance("director_horde_min_distance", "800", FCVAR_CHEAT, "Minimum distance away from the survivors the horde can spawn");
ConVar director_horde_max_distance("director_horde_max_distance", "1500", FCVAR_CHEAT, "Maximum distance away from the survivors the horde can spawn");

void Director::LevelInitPreEntity()
{

}

class CollectAreas
{
	public:
		CollectAreas(const CNavArea *start, const CBasePlayer *player, const Vector &south, const Vector &north)
			: start(start), player(player), south(south), north(north) { }

		bool operator()(CNavArea *area)
		{
			TerrorNavArea *terrorarea = static_cast<TerrorNavArea *>(area);

			const Vector &eye = const_cast<CBasePlayer *>(player)->EyePosition();

			if(terrorarea->IsPartiallyVisible(eye, player))
				return true;

			if(terrorarea->IsDamaging() || terrorarea->IsUnderwater())
				return true;

			if(terrorarea->GetAttributes() & (NAV_MESH_CROUCH|NAV_MESH_CLIFF|TerrorNavArea::NAV_BREAKABLEWALL))
				return true;

			if(terrorarea->GetSpawnAttributes() & (TerrorNavArea::PLAYER_START|TerrorNavArea::CHECKPOINT|TerrorNavArea::EMPTY|TerrorNavArea::NO_MOBS))
				return true;

			NavAreaVector &northvec = director.m_northCandidateAreas;
			NavAreaVector &southvec = director.m_southCandidateAreas;

			if(northvec.HasElement(terrorarea) || southvec.HasElement(terrorarea))
				return true;

			const Vector &center = terrorarea->GetCenter();

			int debug = director_debug.GetInt();
			float time = director_candidate_interval.GetFloat();

			if(center.y >= south.y) {
				northvec.AddToTail(terrorarea);
				if(debug == 3)
					terrorarea->DrawFilled(32, 32, 128, 10, time, true, 5.0f);
			}

			if(center.y <= north.y) {
				southvec.AddToTail(terrorarea);
				if(debug == 3)
					terrorarea->DrawFilled(128, 32, 32, 10, time, true, 5.0f);
			}

			return true;
		}

	private:
		const CBasePlayer *player = nullptr;
		const CNavArea *start = nullptr;
		const Vector &north;
		const Vector &south;
};

void Director::FrameUpdatePostEntityThink()
{
	if(m_CandidateUpdateTimer.HasStarted() && !m_CandidateUpdateTimer.IsElapsed())
		return;

	m_CandidateUpdateTimer.Start(director_candidate_interval.GetFloat());

	m_northCandidateAreas.Purge();
	m_southCandidateAreas.Purge();

	Vector vecSouthSurvivor = vec3_origin;
	Vector vecNorthSurvivor = vec3_origin;

	int c = gpGlobals->maxClients;
	if(c <= 0)
		return;

	int i = 1;
	for(; i <= c; i++) {
		CBasePlayer *player = UTIL_PlayerByIndex(i);
		if(!player || !player->IsAlive() || player->GetTeamNumber() != TEAM_SURVIVOR)
			continue;

		const Vector &origin = player->GetAbsOrigin();

		if(vecSouthSurvivor == vec3_origin || vecSouthSurvivor.y > origin.y)
			vecSouthSurvivor = origin;

		if(vecNorthSurvivor == vec3_origin || vecNorthSurvivor.y < origin.y)
			vecNorthSurvivor = origin;
	}

	i = 1;
	for(; i <= c; i++) {
		CBasePlayer *player = UTIL_PlayerByIndex(i);
		if(!player || !player->IsAlive() || player->GetTeamNumber() != TEAM_SURVIVOR)
			continue;

		CNavArea *area = player->GetLastKnownArea();
		if(!area)
			continue;

		CollectAreas scan(area, player, vecSouthSurvivor, vecNorthSurvivor);
		area->ForAllPotentiallyVisibleAreas(scan);
	}
}