#include "cbase.h"
#include "Infected.h"
#include "InfectedBody.h"
#include "InfectedLocomotion.h"
#include "InfectedVision.h"
#include "l4dce_shareddefs.h"
#include "ActionShared.h"
#include "activitylist.h"
#include "tier0/memdbgon.h"

Activity ACT_EXP_ANGRY = ACT_INVALID;
Activity ACT_EXP_IDLE = ACT_INVALID;

static ConVar z_health("z_health", "50", FCVAR_CHEAT);

LINK_ENTITY_TO_CLASS(infected, Infected);
PRECACHE_REGISTER(infected);

ILocomotion *Infected::GetLocomotionInterface() const { return m_pLocomotion; }
IBody *Infected::GetBodyInterface() const { return m_pBody; }
IVision *Infected::GetVisionInterface() const { return m_pVision; }

Infected::Infected()
	: BaseClass()
{
	m_pVision = new InfectedVision(this);
	m_pBody = new InfectedBody(this);
	m_pLocomotion = new InfectedLocomotion(this);

	ALLOCATE_INTENTION_INTERFACE(Infected);
}

Infected::~Infected()
{
	DEALLOCATE_INTENTION_INTERFACE();

	if(m_pLocomotion) {
		m_pLocomotion->Reset();
		delete m_pLocomotion;
	}
	m_pLocomotion = nullptr;

	if(m_pBody) {
		m_pBody->Reset();
		delete m_pBody;
	}
	m_pBody = nullptr;

	if(m_pVision) {
		m_pVision->Reset();
		delete m_pVision;
	}
	m_pVision = nullptr;
}

Vector Infected::EyePosition()
{
	return m_vecEye;
}

void Infected::Update()
{
	BaseClass::Update();

	GetAttachment(m_nForward, m_vecEye, m_angEye);
}

void Infected::OnModelChanged()
{
	BaseClass::OnModelChanged();

	m_nForward = LookupAttachment("forward");
}

void Infected::Precache()
{
	BaseClass::Precache();

	PrecacheModel("models/infected/limbs/limb_male_head01.mdl");
	PrecacheModel("models/infected/limbs/limb_male_rarm01.mdl");
	PrecacheModel("models/infected/limbs/limb_male_larm01.mdl");
	PrecacheModel("models/infected/limbs/limb_male_rleg01.mdl");
	PrecacheModel("models/infected/limbs/limb_male_lleg01.mdl");
	PrecacheModel("models/infected/gibs/gibs.mdl");

	PrecacheScriptSound("Zombie.Sleeping");
	PrecacheScriptSound("Zombie.Wander");
	PrecacheScriptSound("Zombie.BecomeEnraged");
	PrecacheScriptSound("Zombie.Rage");
	PrecacheScriptSound("Zombie.RageAtVictim");
	PrecacheScriptSound("Zombie.BecomeAlert");
	PrecacheScriptSound("Zombie.Alert");
	PrecacheScriptSound("Zombie.Shoved");
	PrecacheScriptSound("Zombie.AttackMiss");
	PrecacheScriptSound("Zombie.ClawScrape");
	PrecacheScriptSound("Zombie.Die");
	PrecacheScriptSound("Zombie.IgniteScream");
	PrecacheScriptSound("Zombie.Shot");
	PrecacheScriptSound("Zombie.Punch");
	PrecacheScriptSound("Zombie.BulletImpact");
	PrecacheScriptSound("Zombie.HeadlessCough");
	PrecacheScriptSound("MegaMobIncoming");

	PrecacheParticleSystem("blood_atomized");

	ACT_EXP_ANGRY = ActivityList_RegisterPrivateActivity("ACT_EXP_ANGRY");
	ACT_EXP_IDLE = ActivityList_RegisterPrivateActivity("ACT_EXP_IDLE");
}

void Infected::Spawn()
{
	SetHealth(z_health.GetInt());

	BaseClass::Spawn();

	ChangeTeam(TEAM_INFECTED);

	const char *model = "models/infected/common_male_tanktop_jeans.mdl";
	
	if(!engine->IsModelPrecached(model))
		PrecacheModel(model);

	SetModel(model);
}

bool Infected::IsGroundLevel(float radius) const
{
	IBody *body = GetBodyInterface();
	return UTIL_IsGroundLevel(radius, GetPosition(), body->GetHullHeight(), body->GetSolidMask(), this);
}

IMPLEMENT_INTENTION_INTERFACE(Infected, InfectedExecAction);

Behavior<Infected> *Infected::GetBehavior() const
{
	return static_cast<Behavior<Infected> *>(m_pIntention->FirstContainedResponder());
}

CON_COMMAND_F(ent_create_infected, "spawns a infected at your crosshair", FCVAR_CHEAT)
{
	CBasePlayer *player = UTIL_GetCommandClient();
	if(!player)
		return;

	Vector forward;
	player->EyeVectors(&forward);

	trace_t result;
	UTIL_TraceLine(player->EyePosition(), player->EyePosition() + MAX_TRACE_LENGTH * forward, MASK_BLOCKLOS_AND_NPCS|CONTENTS_IGNORE_NODRAW_OPAQUE, player, COLLISION_GROUP_NONE, &result);
	if(!result.DidHit())
		return;

	Infected *infected = static_cast<Infected *>(CreateEntityByName("infected"));
	if(infected) {
		Vector forward = player->GetAbsOrigin() - result.endpos;
		forward.z = 0.0f;
		forward.NormalizeInPlace();

		QAngle angles;
		VectorAngles(forward, angles);

		Vector origin = (result.endpos + Vector(0, 0, 18.0f));
		infected->Teleport(&origin, &angles, nullptr);

		DispatchSpawn(infected);
	}
}

/*
static const char *const infectedmodels[] =
{
"models/infected/common_female_tanktop_jeans.mdl",
"models/infected/common_female_tanktop_jeans_rain.mdl",
//"models/infected/common_female_tanktop_jeans_swamp.mdl",
"models/infected/common_female_tshirt_skirt.mdl",
"models/infected/common_female_tshirt_skirt_swamp.mdl",
"models/infected/common_male_dressshirt_jeans.mdl",
"models/infected/common_male_polo_jeans.mdl",
"models/infected/common_male_tanktop_jeans.mdl",
"models/infected/common_male_tanktop_jeans_rain.mdl",
"models/infected/common_male_tanktop_jeans_swamp.mdl",
"models/infected/common_male_tanktop_overalls.mdl",
"models/infected/common_male_tanktop_overalls_rain.mdl",
"models/infected/common_male_tanktop_overalls_swamp.mdl",
"models/infected/common_male_tshirt_cargos.mdl",
"models/infected/common_male_tshirt_cargos_swamp.mdl",
"models/infected/common_male_ceda.mdl",
"models/infected/common_male_clown.mdl",
"models/infected/common_male_fallen_survivor.mdl",
"models/infected/common_male_mud.mdl",
"models/infected/common_male_riot.mdl",
"models/infected/common_male_roadcrew.mdl",
"models/infected/common_male_roadcrew_rain.mdl",
};
*/