#include "cbase.h"
#include "InfectedBody.h"
#include "Infected.h"
#include "activitylist.h"
#include "tier0/memdbgon.h"

extern ConVar nb_saccade_time;
extern ConVar nb_saccade_speed;
extern ConVar nb_head_aim_steady_max_rate;
extern ConVar nb_head_aim_settle_duration;
extern ConVar nb_head_aim_resettle_angle;
extern ConVar nb_head_aim_resettle_time;

InfectedBody::InfectedBody(Infected *infected)
	: BaseClass(infected), m_pInfected(infected)
{
	
}

int InfectedBody::SelectAnimationSequence(Activity act) const
{
	if(act == ACT_INVALID)
		return -1;

	return m_pInfected->SelectWeightedSequence(act);
}

Activity InfectedBody::GetActivity() const
{
	return m_pInfected->GetSequenceActivity(m_pInfected->GetSequence());
}

bool InfectedBody::IsActivity(Activity act) const
{
	return (GetActivity() == act);
}

bool InfectedBody::HasActivityType(unsigned int flags) const
{
	return !!(m_nActivityFlags & flags);
}

unsigned int InfectedBody::GetSolidMask() const
{
	return m_pInfected->PhysicsSolidMaskForEntity();
}

unsigned int InfectedBody::GetCollisionGroup() const
{
	return m_pInfected->GetCollisionGroup();
}

void InfectedBody::SetArousal(ArousalType arousal)
{
	m_nArousal = arousal;
}

IBody::ArousalType InfectedBody::GetArousal() const
{
	return m_nArousal;
}

bool InfectedBody::IsArousal(ArousalType arousal) const
{
	return (GetArousal() == arousal);
}

void InfectedBody::SetDesiredPosture(PostureType posture)
{
	if(IsPostureChanging() || !IsInDesiredPosture())
		return;

	Activity idealActivity = ACT_INVALID;

	if(posture == STAND) {
		if(GetActualPosture() == SIT) {
			if(IsArousal(NEUTRAL))
				idealActivity = ACT_TERROR_SIT_TO_STAND;
			else
				idealActivity = ACT_TERROR_SIT_TO_STAND_ALERT;
		}
		else if(GetActualPosture() == LIE) {
			if(IsArousal(NEUTRAL))
				idealActivity = ACT_TERROR_LIE_TO_STAND;
			else
				idealActivity = ACT_TERROR_LIE_TO_STAND_ALERT;
		}
	} else if(posture == SIT) {
		if(GetActualPosture() == STAND)
			idealActivity = ACT_TERROR_SIT_FROM_STAND;
		else if(GetActualPosture() == LIE)
			idealActivity = ACT_TERROR_LIE_TO_SIT;
	} else if(posture == LIE) {
		if(GetActualPosture() == STAND)
			idealActivity = ACT_TERROR_LIE_FROM_STAND;
		else if(GetActualPosture() == SIT)
			idealActivity = ACT_TERROR_SIT_TO_LIE;
	}

	if(idealActivity != ACT_INVALID) {
		m_nPostureActvity = idealActivity;
		StartActivity(idealActivity, ACTIVITY_UNINTERRUPTIBLE | MOTION_CONTROLLED_XY);
		m_bChangingPosture = true;
	} else {
		m_nPostureActvity = ACT_INVALID;
		m_bChangingPosture = false;
		m_nActualPosture = posture;
		m_pInfected->OnPostureChanged();
	}

	m_nDesiredPosture = posture;
}

IBody::PostureType InfectedBody::GetDesiredPosture() const
{
	return m_nDesiredPosture;
}

bool InfectedBody::IsDesiredPosture(PostureType posture) const
{
	return (GetActualPosture() == posture);
}

bool InfectedBody::IsInDesiredPosture() const
{
	return (GetActualPosture() == GetDesiredPosture());
}

IBody::PostureType InfectedBody::GetActualPosture() const
{
	return m_nActualPosture;
}

bool InfectedBody::IsActualPosture(PostureType posture) const
{
	return (GetActualPosture() == posture);
}

bool InfectedBody::IsPostureMobile() const
{
	PostureType posture = GetActualPosture();
	if(posture == SIT || posture == LIE)
		return false;

	return !IsPostureChanging();
}

bool InfectedBody::IsPostureChanging() const
{
	return m_bChangingPosture;
}

void InfectedBody::OnPostureChanged()
{
	BaseClass::OnPostureChanged();

	Activity idealActivity = ACT_INVALID;

	if(GetActualPosture() == SIT)
		idealActivity = ACT_TERROR_SIT_IDLE;
	else if(GetActualPosture() == LIE)
		idealActivity = ACT_TERROR_LIE_IDLE;

	if(idealActivity != ACT_INVALID)
		StartActivity(idealActivity, MOTION_CONTROLLED_XY);
}

void InfectedBody::OnAnimationActivityComplete(int activity)
{
	BaseClass::OnAnimationActivityComplete(activity);

	if(m_pInfected->IsDebugging(NEXTBOT_ANIMATION))
		DevMsg("%3.2f: bot(#%d) InfectedBody::OnAnimationActivityComplete: %i %s\n", gpGlobals->curtime, m_pInfected->entindex(), activity, ActivityList_NameForIndex(activity));

	if(activity == m_nPostureActvity) {
		m_nPostureActvity = ACT_INVALID;
		m_bChangingPosture = false;
		m_nActualPosture = m_nDesiredPosture;
		m_pInfected->OnPostureChanged();
	}
}

void InfectedBody::OnAnimationActivityInterrupted(int activity)
{
	BaseClass::OnAnimationActivityInterrupted(activity);

	if(m_pInfected->IsDebugging(NEXTBOT_ANIMATION))
		DevMsg("%3.2f: bot(#%d) InfectedBody::OnAnimationActivityInterrupted: %i %s\n", gpGlobals->curtime, m_pInfected->entindex(), activity, ActivityList_NameForIndex(activity));
}

void InfectedBody::Reset()
{
	BaseClass::Reset();

	//m_pInfected->ResetSequence(0);

	m_nOldActivityFlags = 0;
	m_nActivityFlags = 0;

	m_nOldDesiredActivity = ACT_INVALID;
	m_nDesiredActivity = ACT_INVALID;

	m_nArousal = NEUTRAL;

	m_nPostureActvity = ACT_INVALID;
	m_nActualPosture = STAND;
	m_nDesiredPosture = STAND;
	m_bChangingPosture = false;

	m_lookAtPos = vec3_origin;
	m_lookAtSubject = nullptr;
	m_lookAtReplyWhenAimed = nullptr;
	m_lookAtVelocity = vec3_origin;
	m_lookAtExpireTimer.Invalidate();

	m_lookAtPriority = BORING;
	m_lookAtExpireTimer.Invalidate();
	m_lookAtDurationTimer.Invalidate();
	m_isSightedIn = false;
	m_hasBeenSightedIn = false;
	m_headSteadyTimer.Invalidate();

	m_priorAngles = vec3_angle;
	m_anchorRepositionTimer.Invalidate();
	m_anchorForward = vec3_origin;
}

bool InfectedBody::StartActivity(Activity act, unsigned int flags)
{
	if(act == ACT_INVALID)
		return false;

	if(IsPostureChanging() || !IsInDesiredPosture())
		return false;

	if(HasActivityType(ACTIVITY_UNINTERRUPTIBLE) && !m_pInfected->IsSequenceFinished())
		return false;

	int animDesired = SelectAnimationSequence(act);
	int tried = 0;
	while(m_BadSequences.Find(animDesired) != m_BadSequences.InvalidIndex() && tried <= 10) {
		animDesired = SelectAnimationSequence(act);
		tried++;
	}

	if(tried >= 10)
		return false;

	Activity curact = m_pInfected->GetSequenceActivity(m_pInfected->GetSequence());
	Activity newact = m_pInfected->GetSequenceActivity(animDesired);
	if(curact == newact)
		return false;

	m_nOldActivityFlags = m_nActivityFlags;
	m_nActivityFlags = flags;

	m_nOldDesiredActivity = m_nDesiredActivity;
	m_nDesiredActivity = act;

	if(m_nOldDesiredActivity != act)
		m_pInfected->OnAnimationActivityInterrupted(m_nOldDesiredActivity);

	m_pInfected->ResetSequence(animDesired);
	return true;
}

void InfectedBody::OnModelChanged()
{
	BaseClass::OnModelChanged();

	m_nMoveX = m_pInfected->LookupPoseParameter("move_x");
	m_nMoveY = m_pInfected->LookupPoseParameter("move_y");

	m_nLeanYaw = m_pInfected->LookupPoseParameter("lean_yaw");
	m_nLeanPitch = m_pInfected->LookupPoseParameter("lean_pitch");

	m_nBodyYaw = m_pInfected->LookupPoseParameter("body_yaw");
	m_nBodyPitch = m_pInfected->LookupPoseParameter("body_pitch");

	#pragma message(FILE_LINE_STRING " delete all this and actually fix/remove the anims")
	m_BadSequences.Purge();

	static const char *const badsequences[] = {
		"violent_alert01_Common_D",
		"violent_alert01_Common_E",
		"Idle_Acquire_11",
	};

	for(int i = 0; i < ARRAYSIZE(badsequences); i++) {
		int s = m_pInfected->LookupSequence(badsequences[i]);
		if(s != -1)
			m_BadSequences.AddToTail(i);
	}
}

const Vector &InfectedBody::GetViewVector() const
{
	static Vector view;
	AngleVectors(m_pInfected->EyeAngles(), &view);
	return view;
}

void InfectedBody::Update()
{
	BaseClass::Update();

	Vector forward;
	Vector right;
	m_pInfected->GetVectors(&forward, &right, nullptr);

	ILocomotion *locomotion = m_pInfected->GetLocomotionInterface();
	const Vector &velocity = locomotion->GetVelocity();
	float walkspeed = locomotion->GetWalkSpeed();
	float speed = velocity.Length2D();
	
	float playbackrate = 1.0f;
	if(walkspeed > speed)
		playbackrate = speed / walkspeed;
	playbackrate = clamp(playbackrate, 0.01f, 10.0f);

	const Vector &motion = locomotion->GetGroundMotionVector();
	m_pInfected->SetPoseParameter(m_nMoveX, motion.Dot(forward) * playbackrate);
	m_pInfected->SetPoseParameter(m_nMoveY, motion.Dot(right) * playbackrate);

	const QAngle &lean = locomotion->GetDesiredLean();
	m_pInfected->SetPoseParameter(m_nLeanPitch, lean.x);
	m_pInfected->SetPoseParameter(m_nLeanYaw, lean.y);

	if(HasActivityType(MOTION_CONTROLLED_XY) || HasActivityType(MOTION_CONTROLLED_Z)) {
		bool finished = false;
		Vector pos; QAngle ang;
		float interval = m_pInfected->GetAnimTimeInterval();
		if(m_pInfected->GetIntervalMovement(interval, finished, pos, ang)) {
			if(HasActivityType(MOTION_CONTROLLED_Z) && !HasActivityType(MOTION_CONTROLLED_XY)) {
				pos.x = locomotion->GetFeet().x;
				pos.y = locomotion->GetFeet().y;
			} else if(HasActivityType(MOTION_CONTROLLED_XY) && !HasActivityType(MOTION_CONTROLLED_Z))
				pos.z = locomotion->GetFeet().z;

			locomotion->DriveTo(pos);

			if(HasActivityType(MOTION_CONTROLLED_XY)) {
				QAngle angles = m_pInfected->GetLocalAngles();

				float angleDiff = UTIL_AngleDiff(ang.y, angles.y);
				float deltaYaw = 250.0f * GetUpdateInterval();

				if(angleDiff < -deltaYaw)
					angles.y -= deltaYaw;
				else if(angleDiff > deltaYaw)
					angles.y += deltaYaw;
				else
					angles.y += angleDiff;

				angles.x = ang.x;
				angles.z = ang.z;

				m_pInfected->SetLocalAngles(angles);
			}
		}
	}

	if(m_pInfected->IsSequenceFinished()) {
		if(m_nDesiredActivity != ACT_INVALID)
			m_pInfected->OnAnimationActivityComplete(m_nDesiredActivity);
		if(HasActivityType(ACTIVITY_TRANSITORY)) {
			if(m_nOldDesiredActivity != ACT_INVALID)
				StartActivity(m_nOldDesiredActivity, m_nOldActivityFlags);
		} else if(!HasActivityType(ACTIVITY_UNINTERRUPTIBLE)) {
			if(m_nDesiredActivity != ACT_INVALID)
				m_pInfected->ResetSequence(SelectAnimationSequence(m_nDesiredActivity));
		}
	}

	if(m_pInfected->IsDebugging(NEXTBOT_ANIMATION)) {
		CFmtStr msg;
		int sequence = m_pInfected->GetSequence();
		Activity act = m_pInfected->GetSequenceActivity(sequence);
		m_pInfected->DisplayDebugText(msg.sprintf("%i %s", act, ActivityList_NameForIndex(act)));
		m_pInfected->DisplayDebugText(msg.sprintf("%i %s", sequence, m_pInfected->GetSequenceName(sequence)));
	}

	const float deltaT = GetUpdateInterval();
	if(deltaT < 0.00001f)
		return;

	const QAngle &currentAngles = m_pInfected->EyeAngles();

	bool isSteady = true;
	float actualPitchRate = AngleDiff(currentAngles.x, m_priorAngles.x);
	if(abs(actualPitchRate) > nb_head_aim_steady_max_rate.GetFloat() * deltaT) {
		isSteady = false;
	} else {
		float actualYawRate = AngleDiff(currentAngles.y, m_priorAngles.y);
		if(abs(actualYawRate) > nb_head_aim_steady_max_rate.GetFloat() * deltaT)
			isSteady = false;
	}

	m_priorAngles = currentAngles;

	if(isSteady) {
		if(!m_headSteadyTimer.HasStarted())
			m_headSteadyTimer.Start();
	} else
		m_headSteadyTimer.Invalidate();

	const Vector &eyepos = GetEyePosition();

	if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT)) {
		if(IsHeadSteady()) {
			float t = GetHeadSteadyDuration() / 3.0f;
			t = clamp(t, 0.0f, 1.0f);
			NDebugOverlay::Circle(eyepos, t * 10.0f, 0, 255, 0, 255, true, 2.0f * deltaT);
		}
	}

	if(m_hasBeenSightedIn && m_lookAtExpireTimer.IsElapsed())
		return;

	const Vector &view = GetViewVector();
	float deltaAngle = RAD2DEG(acos(DotProduct(view, m_anchorForward)));
	if(deltaAngle > nb_head_aim_resettle_angle.GetFloat()) {
		m_anchorRepositionTimer.Start(RandomFloat(0.9f, 1.1f) * nb_head_aim_resettle_time.GetFloat());
		m_anchorForward = view;
		return;
	}

	if(m_anchorRepositionTimer.HasStarted() && !m_anchorRepositionTimer.IsElapsed())
		return;

	m_anchorRepositionTimer.Invalidate();

	CBaseEntity *subject = m_lookAtSubject;
	if(subject) {
		if(m_lookAtTrackingTimer.IsElapsed()) {
			Vector desiredLookAtPos;
			if(subject->MyCombatCharacterPointer())
				desiredLookAtPos = m_pInfected->GetIntentionInterface()->SelectTargetPoint(m_pInfected, subject->MyCombatCharacterPointer());
			else
				desiredLookAtPos = subject->WorldSpaceCenter();
			desiredLookAtPos += GetHeadAimSubjectLeadTime() * subject->GetAbsVelocity();
			Vector errorVector = desiredLookAtPos - m_lookAtPos;
			float error = errorVector.NormalizeInPlace();
			float trackingInterval = GetHeadAimTrackingInterval();
			if(trackingInterval < deltaT)
				trackingInterval = deltaT;
			float errorVel = error / trackingInterval;
			m_lookAtVelocity = (errorVel * errorVector) + subject->GetAbsVelocity();
			m_lookAtTrackingTimer.Start(RandomFloat(0.8f, 1.2f) * trackingInterval);
		}
		m_lookAtPos += deltaT * m_lookAtVelocity;
	}

	Vector to = (m_lookAtPos - eyepos);
	to.NormalizeInPlace();

	if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT)) {
		NDebugOverlay::Line(eyepos, eyepos + 100.0f * view, 255, 255, 0, false, 2.0f * deltaT);
		float thickness = isSteady ? 2.0f : 3.0f;
		int r = m_isSightedIn ? 255 : 0;
		int g = subject ? 255 : 0;
		NDebugOverlay::HorzArrow(eyepos, m_lookAtPos, thickness, r, g, 255, 255, false, 2.0f * deltaT);
	}

	float dot = DotProduct(view, to);
	if(dot > 0.98f) {
		m_isSightedIn = true;
		if(!m_hasBeenSightedIn) {
			m_hasBeenSightedIn = true;
			if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT))
				ConColorMsg(Color(255, 100, 0, 255), "%3.2f: %s Look At SIGHTED IN\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier());
		}
		if(m_lookAtReplyWhenAimed) {
			m_lookAtReplyWhenAimed->OnSuccess(m_pInfected);
			m_lookAtReplyWhenAimed = nullptr;
		}
	} else
		m_isSightedIn = false;

	float approachRate = GetMaxHeadAngularVelocity();

	const float easeOut = 0.7f;
	if(dot > easeOut) {
		float t = RemapVal(dot, easeOut, 1.0f, 1.0f, 0.02f);
		approachRate *= sin(1.57f * t);
	}

	const float easeInTime = 0.25f;
	if(m_lookAtDurationTimer.GetElapsedTime() < easeInTime)
		approachRate *= m_lookAtDurationTimer.GetElapsedTime() / easeInTime;

	QAngle desiredAngles;
	VectorAngles(to, desiredAngles);

	QAngle angles;
	angles.y = ApproachAngle(desiredAngles.y, currentAngles.y, approachRate * deltaT);
	angles.x = ApproachAngle(desiredAngles.x, currentAngles.x, 0.5f * approachRate * deltaT);
	angles.z = 0.0f;

	angles.x = AngleNormalize(angles.x);
	angles.y = AngleNormalize(angles.y);

	angles.x = clamp(angles.x, -90.0f, 45.0f);
	angles.y = clamp(angles.y, -180.0f, 180.0f);

	#pragma message(FILE_LINE_STRING " find out how to math body_yaw/body_pitch")

	if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT)) {
		CFmtStr msg;
		m_pInfected->DisplayDebugText(msg.sprintf("%f %f", angles.x, angles.y));

		m_pInfected->SetPoseParameter(m_nBodyPitch, -angles.x);
		m_pInfected->SetPoseParameter(m_nBodyYaw, -angles.y);
	}
}

float InfectedBody::GetMaxHeadAngularVelocity() const
{
	return nb_saccade_speed.GetFloat();
}

void InfectedBody::ClearPendingAimReply()
{
	m_lookAtReplyWhenAimed = nullptr;
}

float InfectedBody::GetHeadSteadyDuration() const
{
	//return IsHeadAimingOnTarget() ? m_headSteadyTimer.GetElapsedTime() : 0.0f;
	return m_headSteadyTimer.HasStarted() ? m_headSteadyTimer.GetElapsedTime() : 0.0f;
}

bool InfectedBody::IsHeadSteady() const
{
	return m_headSteadyTimer.HasStarted();
}

bool InfectedBody::IsHeadAimingOnTarget() const
{
	return m_isSightedIn;
}

void InfectedBody::AimHeadTowards(const Vector &lookAtPos, LookAtPriorityType priority, float duration, INextBotReply *replyWhenAimed, const char *reason)
{
	if(duration <= 0.0f)
		duration = 0.1f;

	if(m_lookAtPriority == priority) {
		if(!IsHeadSteady() || GetHeadSteadyDuration() < nb_head_aim_settle_duration.GetFloat()) {
			if(replyWhenAimed)
				replyWhenAimed->OnFail(m_pInfected, INextBotReply::DENIED);
			if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT))
				ConColorMsg(Color(255, 0, 0, 255), "%3.2f: %s Look At '%s' rejected - previous aim not %s\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), reason, IsHeadSteady() ? "settled long enough" : "head-steady");
			return;
		}
	}

	if(m_lookAtPriority > priority && !m_lookAtExpireTimer.IsElapsed()) {
		if(replyWhenAimed)
			replyWhenAimed->OnFail(m_pInfected, INextBotReply::DENIED);
		if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT))
			ConColorMsg(Color(255, 0, 0, 255), "%3.2f: %s Look At '%s' rejected - higher priority aim in progress\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), reason);
		return;
	}

	if(m_lookAtReplyWhenAimed)
		m_lookAtReplyWhenAimed->OnFail(m_pInfected, INextBotReply::INTERRUPTED);

	m_lookAtReplyWhenAimed = replyWhenAimed;
	m_lookAtExpireTimer.Start(duration);

	if((m_lookAtPos - lookAtPos).IsLengthLessThan(1.0f)) {
		m_lookAtPriority = priority;
		return;
	}

	m_lookAtPos = lookAtPos;
	m_lookAtSubject = nullptr;
	m_lookAtPriority = priority;
	m_lookAtDurationTimer.Start();
	//m_isSightedIn = false;
	m_hasBeenSightedIn = false;

	if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT)) {
		NDebugOverlay::Cross3D(lookAtPos, 2.0f, 255, 255, 100, true, 2.0f * duration);
		const char *priName = "";
		switch(priority) {
			case BORING:		priName = "BORING"; break;
			case INTERESTING:	priName = "INTERESTING"; break;
			case IMPORTANT:		priName = "IMPORTANT"; break;
			case CRITICAL:		priName = "CRITICAL"; break;
		}
		ConColorMsg(Color(255, 100, 0, 255), "%3.2f: %s Look At ( %g, %g, %g ) for %3.2f s, Pri = %s, Reason = %s\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), lookAtPos.x, lookAtPos.y, lookAtPos.z, duration, priName, (reason) ? reason : "");
	}
}

void InfectedBody::AimHeadTowards(CBaseEntity *subject, LookAtPriorityType priority, float duration, INextBotReply *replyWhenAimed, const char *reason)
{
	if(duration <= 0.0f)
		duration = 0.1f;

	if(subject == nullptr)
		return;

	if(m_lookAtPriority == priority) {
		if(!IsHeadSteady() || GetHeadSteadyDuration() < nb_head_aim_settle_duration.GetFloat()) {
			if(replyWhenAimed)
				replyWhenAimed->OnFail(m_pInfected, INextBotReply::DENIED);
			if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT))
				ConColorMsg(Color(255, 0, 0, 255), "%3.2f: %s Look At '%s' rejected - previous aim not %s\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), reason, IsHeadSteady() ? "head-steady" : "settled long enough");
			return;
		}
	}

	if(m_lookAtPriority > priority && !m_lookAtExpireTimer.IsElapsed()) {
		if(replyWhenAimed)
			replyWhenAimed->OnFail(m_pInfected, INextBotReply::DENIED);
		if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT))
			ConColorMsg(Color(255, 0, 0, 255), "%3.2f: %s Look At '%s' rejected - higher priority aim in progress\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), reason);
		return;
	}

	if(m_lookAtReplyWhenAimed)
		m_lookAtReplyWhenAimed->OnFail(m_pInfected, INextBotReply::INTERRUPTED);

	m_lookAtReplyWhenAimed = replyWhenAimed;
	m_lookAtExpireTimer.Start(duration);

	if(subject == m_lookAtSubject) {
		m_lookAtPriority = priority;
		return;
	}

	m_lookAtSubject = subject;
	m_lookAtPriority = priority;
	m_lookAtDurationTimer.Start();
	//m_isSightedIn = false;
	m_hasBeenSightedIn = false;

	if(m_pInfected->IsDebugging(NEXTBOT_LOOK_AT)) {
		NDebugOverlay::Cross3D(m_lookAtPos, 2.0f, 100, 100, 100, true, duration);
		const char *priName = "";
		switch(priority) {
			case BORING:		priName = "BORING"; break;
			case INTERESTING:	priName = "INTERESTING"; break;
			case IMPORTANT:		priName = "IMPORTANT"; break;
			case CRITICAL:		priName = "CRITICAL"; break;
		}
		ConColorMsg(Color(255, 100, 0, 255), "%3.2f: %s Look At subject %s for %3.2f s, Pri = %s, Reason = %s\n", gpGlobals->curtime, m_pInfected->GetDebugIdentifier(), subject->GetClassname(), duration, priName, (reason) ? reason : "");
	}
}