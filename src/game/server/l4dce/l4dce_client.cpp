#include "cbase.h"
#include "l4dce_player.h"
#include "l4dce_gamerules.h"
#include "tier1/KeyValues.h"
#include "tier0/memdbgon.h"

extern ConVar teamplay;
extern bool g_fGameOver;

void ClientPutInServer(edict_t *pEdict, const char *playername)
{
	CTerrorPlayer *pPlayer = CTerrorPlayer::CreatePlayer("player", pEdict);
	pPlayer->SetPlayerName(playername);
}

void ClientActive(edict_t *pEdict, bool bLoadGame)
{
	CTerrorPlayer *pPlayer = dynamic_cast<CTerrorPlayer *>(CBaseEntity::Instance(pEdict));
	Assert(pPlayer);

	if(!pPlayer)
		return;

	pPlayer->InitialSpawn();

	if(!bLoadGame)
		pPlayer->Spawn();
}

void ClientFullyConnect(edict_t *pEntity)
{

}

const char *GetGameDescription()
{
	if(g_pGameRules)
		return g_pGameRules->GetGameDescription();
	else
		return "Left 4 Dead: Community Edition";
}

CBaseEntity *FindEntity(edict_t *pEdict, char *classname)
{
	if(FStrEq(classname, ""))
	{
		CBasePlayer *pPlayer = static_cast<CBasePlayer *>(GetContainingEntity(pEdict));

		if(pPlayer)
			return pPlayer->FindPickerEntityClass(classname);
	}

	return nullptr;
}

PRECACHE_REGISTER_BEGIN(GLOBAL, ClientGamePrecache);
	PRECACHE(KV_DEP_FILE, "resource/l4dce_precache.txt");
	PRECACHE(KV_DEP_FILE, "resource/ParticleEmitters.txt");
PRECACHE_REGISTER_END();

void ClientGamePrecache()
{
	
}

void respawn(CBaseEntity *pEdict, bool fCopyCorpse)
{
	if(gpGlobals->coop || gpGlobals->deathmatch)
	{
		if(fCopyCorpse)
			((CTerrorPlayer *)pEdict)->CreateCorpse();

		pEdict->Spawn();
	}
	else
	{
		engine->ServerCommand("reload\n");
	}
}

void GameStartFrame( void )
{
	VPROF("GameStartFrame()");

	if(g_fGameOver)
		return;

	gpGlobals->teamplay = (teamplay.GetInt() != 0);
}

void InstallGameRules()
{
	CreateGameRulesObject("CTerrorGameRules");
}