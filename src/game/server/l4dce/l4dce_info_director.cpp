#include "cbase.h"
#include "l4dce_info_director.h"
#include "l4dce_player.h"
#include "l4dce_spawnpoint.h"

BEGIN_DATADESC( CInfoDirector )
	DEFINE_INPUTFUNC( FIELD_VOID, "PanicEvent", InputPanicEvent ),
	DEFINE_INPUTFUNC( FIELD_INTEGER, "PanicEventControlled", InputPanicEventControlled ),
	DEFINE_INPUTFUNC( FIELD_VOID, "ForceSurvivorPositions", InputForceSurvivorPositions ),
	DEFINE_INPUTFUNC( FIELD_VOID, "ReleaseSurvivorPositions", InputReleaseSurvivorPositions ),
	DEFINE_INPUTFUNC( FIELD_STRING, "FireConceptToAny", InputFireConceptToAny ),
	DEFINE_INPUTFUNC( FIELD_VOID, "StartIntro", InputStartIntro ),
	DEFINE_INPUTFUNC( FIELD_VOID, "FinishIntro", InputFinishIntro ),
	DEFINE_INPUTFUNC( FIELD_VOID, "EnableTankFrustration", InputEnableTankFrustration ),
	DEFINE_INPUTFUNC( FIELD_VOID, "DisableTankFrustration", InputDisableTankFrustration ),

	DEFINE_OUTPUT( m_OnGamePlayStart, "OnGameplayStart" ),
	DEFINE_OUTPUT( m_OnPanicEventFinished, "OnPanicEventFinished" )
END_DATADESC()

LINK_ENTITY_TO_CLASS( info_director, CInfoDirector )

void CInfoDirector::InputPanicEvent( inputdata_t &inputdata )
{

}

void CInfoDirector::InputPanicEventControlled( inputdata_t &inputdata )
{

}

void CInfoDirector::InputForceSurvivorPositions( inputdata_t &inputdata )
{
	for (int i = 0; i <= gpGlobals->maxClients; i++ )
	{
		CTerrorPlayer *pPlayer = ToTerrorPlayer(UTIL_PlayerByIndex( i ));
		
		if ( !pPlayer || !pPlayer->IsAlive() )
			continue;

		CSurvivorPosition *pSurvivorPosition = (CSurvivorPosition *)gEntList.FindEntityByClassname(NULL, "info_survivor_position");
		while ( pSurvivorPosition )
		{
			if ( pPlayer->GetSurvivorIndex() != pSurvivorPosition->GetSurvivorIndex() )
			{
				pSurvivorPosition = (CSurvivorPosition *)gEntList.FindEntityByClassname(pSurvivorPosition, "info_survivor_position");
				continue;
			}

			Vector velocity = vec3_origin;
			pPlayer->Teleport(&pSurvivorPosition->GetAbsOrigin(), &pSurvivorPosition->GetAbsAngles(), &velocity);
			pPlayer->AddFlag(FL_FROZEN);
			break;
		}
	}
}

void CInfoDirector::InputReleaseSurvivorPositions( inputdata_t &inputdata )
{
	for (int i = 0; i<=gpGlobals->maxClients; i++ )
	{
		CTerrorPlayer *pPlayer = ToTerrorPlayer(UTIL_PlayerByIndex( i ));
		
		if ( !pPlayer || !pPlayer->IsAlive() )
			continue;

		pPlayer->RemoveFlag(FL_FROZEN);
	}
}

void CInfoDirector::InputFireConceptToAny( inputdata_t &inputdata )
{

}

void CInfoDirector::InputStartIntro( inputdata_t &inputdata )
{

}

void CInfoDirector::InputFinishIntro( inputdata_t &inputdata )
{

}

void CInfoDirector::InputEnableTankFrustration( inputdata_t &inputdata )
{

}

void CInfoDirector::InputDisableTankFrustration( inputdata_t &inputdata )
{

}