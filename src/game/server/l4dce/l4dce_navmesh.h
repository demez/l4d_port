#ifndef _L4DCE_NAVMESH_H_
#define _L4DCE_NAVMESH_H_

#pragma once

#include "nav_mesh.h"

class TerrorNavArea;

class TerrorNavMesh : public CNavMesh
{
	private:
		DECLARE_CLASS_GAMEROOT(TerrorNavMesh, CNavMesh);

	public:
		virtual void SaveCustomData(CUtlBuffer &fileBuffer) const;
		virtual void LoadCustomData(CUtlBuffer &fileBuffer, unsigned int subVersion);
		virtual void SaveCustomDataPreArea(CUtlBuffer &fileBuffer) const;
		virtual void LoadCustomDataPreArea(CUtlBuffer &fileBuffer, unsigned int subVersion);
		virtual NavErrorType Load();
		virtual unsigned int GetSubVersionNumber() const;
		virtual CNavArea *CreateArea() const;

		void OnAreaCleared(TerrorNavArea *area);

		void LoadFogPlaceDatabase();
};

extern TerrorNavMesh *TheTerrorNavMesh;

#endif