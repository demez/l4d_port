#include "cbase.h"
#include "player_command.h"
#include "player.h"
#include "igamemovement.h"
#include "ipredictionsystem.h"
#include "iservervehicle.h"
#include "vehicle_base.h"
#include "gamestats.h"
#include "tier0/memdbgon.h"

static CMoveData g_MoveData;
CMoveData *g_pMoveData = &g_MoveData;

IPredictionSystem *IPredictionSystem::g_pPredictionSystems = nullptr;

extern IGameMovement *g_pGameMovement;

class CTerrorPlayerMove : public CPlayerMove
{
	private:
		DECLARE_CLASS(CTerrorPlayerMove, CPlayerMove);

	public:
		CTerrorPlayerMove();

		virtual void SetupMove(CBasePlayer *player, CUserCmd *ucmd, IMoveHelper *pHelper, CMoveData *move);
		virtual void FinishMove(CBasePlayer *player, CUserCmd *ucmd, CMoveData *move);

	private:
		Vector m_vecSaveOrigin;
		bool m_bWasInVehicle;
		bool m_bVehicleFlipped;
		bool m_bInGodMode;
		bool m_bInNoClip;
};

static CTerrorPlayerMove g_PlayerMove;
CPlayerMove *PlayerMove()
{
	return &g_PlayerMove;
}

CTerrorPlayerMove::CTerrorPlayerMove()
{
	m_bWasInVehicle = false;
	m_bVehicleFlipped = false;
	m_bInGodMode = false;
	m_bInNoClip = false;
	m_vecSaveOrigin.Init();
}

void CTerrorPlayerMove::SetupMove(CBasePlayer *player, CUserCmd *ucmd, IMoveHelper *pHelper, CMoveData *move)
{
	BaseClass::SetupMove(player, ucmd, pHelper, move);

	g_pGameMovement->SetupMovementBounds(move);

	player->m_flForwardMove = ucmd->forwardmove;
	player->m_flSideMove = ucmd->sidemove;

	if(gpGlobals->frametime != 0)
	{
		IServerVehicle *pVehicle = player->GetVehicle();
		if(pVehicle)
		{
			pVehicle->SetupMove(player, ucmd, pHelper, move);

			if(!m_bWasInVehicle)
			{
				m_bWasInVehicle = true;
				m_vecSaveOrigin.Init();
			}
		}
		else
		{
			m_vecSaveOrigin = player->GetAbsOrigin();

			if(m_bWasInVehicle)
			{
				m_bWasInVehicle = false;
			}
		}
	}
}

void CTerrorPlayerMove::FinishMove(CBasePlayer *player, CUserCmd *ucmd, CMoveData *move)
{
	BaseClass::FinishMove(player, ucmd, move);

	if(gpGlobals->frametime != 0)
	{
		float distance = 0.0f;
		IServerVehicle *pVehicle = player->GetVehicle();
		if(pVehicle)
		{
			pVehicle->FinishMove(player, ucmd, move);
			IPhysicsObject *obj = player->GetVehicleEntity()->VPhysicsGetObject();
			if(obj)
			{
				Vector newPos;
				obj->GetPosition(&newPos, nullptr);
				distance = VectorLength(newPos - m_vecSaveOrigin);
				if(m_vecSaveOrigin == vec3_origin || distance > 100.0f)
					distance = 0.0f;
				m_vecSaveOrigin = newPos;
			}
			
			CPropVehicleDriveable *driveable = dynamic_cast<CPropVehicleDriveable *>(player->GetVehicleEntity());
			if(driveable)
			{
				bool bFlipped = driveable->IsOverturned() && (distance < 0.5f);
				if(m_bVehicleFlipped != bFlipped)
				{
					if(bFlipped)
					{
						gamestats->Event_FlippedVehicle(player, driveable);
					}

					m_bVehicleFlipped = bFlipped;
				}
			}
			else
			{
				m_bVehicleFlipped = false;
			}
		}
		else
		{
			m_bVehicleFlipped = false;
			distance = VectorLength(player->GetAbsOrigin() - m_vecSaveOrigin);
		}

		if(distance > 0)
		{
			gamestats->Event_PlayerTraveled(player, distance, pVehicle ? true : false, !pVehicle);
		}
	}

	bool bGodMode = (player->GetFlags() & FL_GODMODE) ? true : false;
	if(m_bInGodMode != bGodMode)
	{
		m_bInGodMode = bGodMode;
		if(bGodMode)
		{
			gamestats->Event_PlayerEnteredGodMode(player);
		}
	}

	bool bNoClip = (player->GetMoveType() == MOVETYPE_NOCLIP);
	if(m_bInNoClip != bNoClip)
	{
		m_bInNoClip = bNoClip;
		if(bNoClip)
		{
			gamestats->Event_PlayerEnteredNoClip(player);
		}
	}
}