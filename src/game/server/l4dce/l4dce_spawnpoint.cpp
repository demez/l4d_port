#include "cbase.h"
#include "l4dce_spawnpoint.h"
#include "l4dce_shareddefs.h"
#include "l4dce_viewcontrol.h"
#include "l4dce_player.h"
#include "tokenset.h"

const tokenset_t<int> s_CharacterIndex[] = {
	{ "Coach",		SURVIVOR_COACH, },
	{ "Nick",	SURVIVOR_NICK, },
	{ "Ellis",	SURVIVOR_ELLIS, },
	{ "Rochelle",	SURVIVOR_ROCHELLE, },
	{ "Zoey", SURVIVOR_ZOEY, },
	{ "Bill", SURVIVOR_BILL, },
	{ "Francis", SURVIVOR_FRANCIS, },
	{ "Louis", SURVIVOR_LOUIS, },
	{ NULL, NULL }
};

LINK_ENTITY_TO_CLASS(info_survivor_position, CSurvivorPosition)

BEGIN_DATADESC( CSurvivorPosition )
	DEFINE_INPUTFUNC( FIELD_STRING, "SetViewControl", InputSetViewControl ),
END_DATADESC()


bool CSurvivorPosition::KeyValue( const char *szKeyName, const char *szValue ) 
{
	if ( FStrEq( szKeyName, "SurvivorName" ) )
	{
		m_nSurvivor = s_CharacterIndex->GetToken(szValue);
	}

	return BaseClass::KeyValue( szKeyName, szValue );
}			

void CSurvivorPosition::InputSetViewControl( inputdata_t &inputdata )
{
	CSurvivorCamera *pViewControl = (CSurvivorCamera *)gEntList.FindEntityByName(NULL, inputdata.value.String());
	if ( !pViewControl )
		return;

	for ( int i = 0; i <= gpGlobals->maxClients; i++ )
	{
		CTerrorPlayer *pPlayer = ToTerrorPlayer(UTIL_PlayerByIndex(i));
		if ( !pPlayer )
			continue;

		if ( pPlayer->GetSurvivorIndex() != GetSurvivorIndex() )
			continue;

		pViewControl->SetPlayer(pPlayer);
		pViewControl->Enable();
	}
}