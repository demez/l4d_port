#ifndef L4DCE_VIEWCONTROL_H
#define L4DCE_VIEWCONTROL_H

#include "triggers.h"

class CSurvivorCamera : public CTriggerCamera
{
public:
	DECLARE_CLASS( CSurvivorCamera, CTriggerCamera );
	DECLARE_DATADESC()
	virtual void Enable( void );
	virtual void Disable( void );
	virtual void SetPlayer( CBasePlayer *pPlayer ) { m_hPlayer = pPlayer; }
	virtual void InputStartMovement( inputdata_t &inputdata );
	virtual void GoToPlayer();
};

#endif