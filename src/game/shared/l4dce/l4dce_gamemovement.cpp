#include "cbase.h"
#include "gamemovement.h"
#include "l4dce_player.h"
#include "l4dce_playeranimstate.h"
#include "in_buttons.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CterrorPlayer C_TerrorPlayer
#endif

class CTerrorGameMovement : public CGameMovement
{
	private:
		DECLARE_CLASS(CTerrorGameMovement, CGameMovement);

	public:
		virtual void HandleDuckingSpeedCrop();
		virtual bool CheckJumpButton();
};

static CTerrorGameMovement g_GameMovement;
IGameMovement *g_pGameMovement = (IGameMovement *)&g_GameMovement;
EXPOSE_SINGLE_INTERFACE_GLOBALVAR(CTerrorGameMovement, IGameMovement, INTERFACENAME_GAMEMOVEMENT, g_GameMovement);

extern ConVar sv_gravity;
extern bool g_bMovementOptimizations;

void CTerrorGameMovement::HandleDuckingSpeedCrop()
{
	m_bSpeedCropped = false;
}

bool CTerrorGameMovement::CheckJumpButton()
{
	if (player->pl.deadflag)
	{
		mv->m_nOldButtons |= IN_JUMP ;	// don't jump again until released
		return false;
	}

	// See if we are waterjumping.  If so, decrement count and return.
	if (player->m_flWaterJumpTime)
	{
		player->m_flWaterJumpTime -= gpGlobals->frametime;
		if (player->m_flWaterJumpTime < 0)
			player->m_flWaterJumpTime = 0;
		
		return false;
	}

	// If we are in the water most of the way...
	if ( player->GetWaterLevel() >= 2 )
	{	
		// swimming, not jumping
		SetGroundEntity( NULL );

		if(player->GetWaterType() == CONTENTS_WATER)    // We move up a certain amount
			mv->m_vecVelocity[2] = 100;
		else if (player->GetWaterType() == CONTENTS_SLIME)
			mv->m_vecVelocity[2] = 80;
		
		// play swiming sound
		if ( player->m_flSwimSoundTime <= 0 )
		{
			// Don't play sound again for 1 second
			player->m_flSwimSoundTime = 1000;
			PlaySwimSound();
		}

		return false;
	}

	// No more effect
 	if (player->GetGroundEntity() == NULL)
	{
		mv->m_nOldButtons |= IN_JUMP;
		return false;		// in air, so no effect
	}

	// Don't allow jumping when the player is in a stasis field.
#ifndef HL2_EPISODIC
	if ( player->m_Local.m_bSlowMovement )
		return false;
#endif

	if ( mv->m_nOldButtons & IN_JUMP )
		return false;		// don't pogo stick

	// Cannot jump will in the unduck transition.
	if ( player->m_Local.m_bDucking && (  player->GetFlags() & FL_DUCKING ) )
		return false;

	// Still updating the eye position.
	if ( player->m_Local.m_nDuckJumpTimeMsecs > 0 )
		return false;


	// In the air now.
    SetGroundEntity( NULL );
	
	player->PlayStepSound( (Vector &)mv->GetAbsOrigin(), player->m_pSurfaceData, 1.0, true );
	
	//MoveHelper()->PlayerSetAnimation( PLAYER_JUMP );

	CTerrorPlayer *pTerrorPlayer = static_cast<CTerrorPlayer *>(player);
	pTerrorPlayer->DoAnimationEvent(PLAYERANIMEVENT_JUMP);

	float flGroundFactor = 1.0f;
	if (player->m_pSurfaceData)
	{
		flGroundFactor = player->m_pSurfaceData->game.jumpFactor; 
	}

	float flMul;
	if ( g_bMovementOptimizations )
	{
#if defined(HL2_DLL) || defined(HL2_CLIENT_DLL)
		Assert( sv_gravity.GetFloat() == 600.0f );
		flMul = 160.0f;	// approx. 21 units.
#else
		Assert( sv_gravity.GetFloat() == 800.0f );
		flMul = 268.3281572999747f;
#endif

	}
	else
	{
		flMul = sqrt(2 * sv_gravity.GetFloat() * GAMEMOVEMENT_JUMP_HEIGHT);
	}

	// Acclerate upward
	// If we are ducking...
	float startz = mv->m_vecVelocity[2];
	if ( (  player->m_Local.m_bDucking ) || (  player->GetFlags() & FL_DUCKING ) )
	{
		// d = 0.5 * g * t^2		- distance traveled with linear accel
		// t = sqrt(2.0 * 45 / g)	- how long to fall 45 units
		// v = g * t				- velocity at the end (just invert it to jump up that high)
		// v = g * sqrt(2.0 * 45 / g )
		// v^2 = g * g * 2.0 * 45 / g
		// v = sqrt( g * 2.0 * 45 )
		mv->m_vecVelocity[2] = flGroundFactor * flMul;  // 2 * gravity * height
	}
	else
	{
		mv->m_vecVelocity[2] += flGroundFactor * flMul;  // 2 * gravity * height
	}

	// Add a little forward velocity based on your current forward velocity - if you are not sprinting.
#if defined( HL2_DLL ) || defined( HL2_CLIENT_DLL )

	bool bAllowBunnyHopperSpeedBoost = ( gpGlobals->maxClients == 1 );


	if ( bAllowBunnyHopperSpeedBoost )
	{
		CHLMoveData *pMoveData = ( CHLMoveData* )mv;
		Vector vecForward;
		AngleVectors( mv->m_vecViewAngles, &vecForward );
		vecForward.z = 0;
		VectorNormalize( vecForward );
		
		// We give a certain percentage of the current forward movement as a bonus to the jump speed.  That bonus is clipped
		// to not accumulate over time.
		float flSpeedBoostPerc = ( !pMoveData->m_bIsSprinting && !player->m_Local.m_bDucked ) ? 0.5f : 0.1f;
		float flSpeedAddition = fabs( mv->m_flForwardMove * flSpeedBoostPerc );
		float flMaxSpeed = mv->m_flMaxSpeed + ( mv->m_flMaxSpeed * flSpeedBoostPerc );
		float flNewSpeed = ( flSpeedAddition + mv->m_vecVelocity.Length2D() );

		// If we're over the maximum, we want to only boost as much as will get us to the goal speed
		if ( flNewSpeed > flMaxSpeed )
		{
			flSpeedAddition -= flNewSpeed - flMaxSpeed;
		}

		if ( mv->m_flForwardMove < 0.0f )
			flSpeedAddition *= -1.0f;

		// Add it on
		VectorAdd( (vecForward*flSpeedAddition), mv->m_vecVelocity, mv->m_vecVelocity );
	}
#endif

	FinishGravity();

	//CheckV( player->CurrentCommandNumber(), "CheckJump", mv->m_vecVelocity );

	mv->m_outJumpVel.z += mv->m_vecVelocity[2] - startz;
	mv->m_outStepHeight += 0.15f;


	bool bSetDuckJump = (gpGlobals->maxClients == 1); //most games we only set duck jump if the game is single player


	// Set jump time.
	if ( bSetDuckJump )
	{
		player->m_Local.m_nJumpTimeMsecs = GAMEMOVEMENT_JUMP_TIME;
		player->m_Local.m_bInDuckJump = true;
	}

#if defined( HL2_DLL )

	if ( xc_uncrouch_on_jump.GetBool() )
	{
		// Uncrouch when jumping
		if ( player->GetToggledDuckState() )
		{
			player->ToggleDuck();
		}
	}

#endif

	// Flag that we jumped.
	mv->m_nOldButtons |= IN_JUMP;	// don't jump again until released
	return true;
}