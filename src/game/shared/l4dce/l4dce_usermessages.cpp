#include "cbase.h"
#include "usermessages.h"
#include "shake.h"
#include "voice_gamemgr.h"
#include "tier0/memdbgon.h"

void RegisterUserMessages()
{
	usermessages->Register("Geiger", 1);
	usermessages->Register("Train", 1);
	usermessages->Register("HudText", -1);
	usermessages->Register("SayText", -1);
	usermessages->Register("SayText2", -1);
	usermessages->Register("TextMsg", -1);
	usermessages->Register("HudMsg", -1);
	usermessages->Register("ResetHUD", 1);
	usermessages->Register("GameTitle", 0);
	usermessages->Register("ItemPickup", -1);
	usermessages->Register("ShowMenu", -1);
	usermessages->Register("Shake", 13);
	usermessages->Register("ShakeDir", -1);
	usermessages->Register("Tilt", 22);
	usermessages->Register("Fade", 10);
	usermessages->Register("VGUIMenu", -1);
	usermessages->Register("Rumble", 3);
	usermessages->Register("Battery", 2);
	usermessages->Register("Damage", -1);
	usermessages->Register("VoiceMask", VOICE_MAX_PLAYERS_DW*4 * 2 + 1);
	usermessages->Register("RequestState", 0);
	usermessages->Register("CloseCaption", -1);
	usermessages->Register("CloseCaptionDirect", -1);
	usermessages->Register("HintText", -1);
	usermessages->Register("KeyHintText", -1);
	usermessages->Register("AmmoDenied", 2);
	usermessages->Register("CreditsMsg", 1);
	usermessages->Register("LogoTimeMsg", 4);
	usermessages->Register("AchievementEvent", -1);
	usermessages->Register("CurrentTimescale", 4);
	usermessages->Register("DesiredTimescale", 13);
}