#ifndef _L4DCE_WEAPON_PARSE_H_
#define _L4DCE_WEAPON_PARSE_H_

#pragma once

#include "weapon_parse.h"
#include "networkvar.h"

class CTerrorWeaponInfo : public FileWeaponInfo_t
{
	private:
		DECLARE_CLASS_GAMEROOT(CTerrorWeaponInfo, FileWeaponInfo_t);

	public:
		CTerrorWeaponInfo();

		virtual void Parse(KeyValues *pKeyValuesData, const char *szWeaponName);

	public:
		//L4D parses
		char	szMuzFlash[MAX_WEAPON_STRING];			//First person muzzle flash
		char	szMuzFlash1P[MAX_WEAPON_STRING];		//First person muzzle flash (same as above...?)
		char	szMuzFlash3P[MAX_WEAPON_STRING];		//Third person muzzle flash
		char	szShellName[MAX_WEAPON_STRING];			//Ejected Shell name
		int		iPPenetrationNumLayers;					//"PenetrationNumLayers"?
		int		iPenetrationPower;						//"PenetrationPower"?
		int		iPenetrationMaxDistance;				//Max allowed distance for penetration (0 = infinite)
		float	flDamage;								//Damage per shot
		int		iMaxRange;								//Max range for bullet to fly
		float	flRangeModifier;						//"RangeModifier"?
		float	flCycleTime;							//Min time between each shot
		char	szDisplayName[MAX_WEAPON_STRING];		//name on hud (not used?)
		char	szDisplayNameCaps[MAX_WEAPON_STRING];	//uppercase name on hud (not used?)
		int		iTier;									//Item teir. 0 = secondaries, 1 = low level primaries, 2 = high level primaries
		char	szResponseName[MAX_WEAPON_STRING];		//weapon Name for ResponseRules
		float	flMusicDynamicSpeed;					//"MusicDynamicSpeed"?
		float	flVerticalPunch;						//Vertical view punch for each shot
		bool	bNewInL4D2;								//Used for L4D1 mutator, if true won't spawn if active
		char	szAddonAttachment[MAX_WEAPON_STRING];	//Attachement location for this weapon when holstered
		int		iBullets;								//bullets to shoot
		char	szViewModelDual[MAX_WEAPON_STRING];		// View model of this weapon (when akimbo)
		char	szWorldPickupModel[MAX_WEAPON_STRING];	// Model of this weapon when on the ground
		char	szWorldModelDual[MAX_WEAPON_STRING];	// Model of this weapon seen carried by the player (when akimbo)
		int		iMaxPlayerSpeed;						//Speed of player with this weapon active (used in L4D or CStrike leftover?)
		float	flSpreadPerShot;						//Spread per bullet fired
		Vector	vecSpreadPerShot;						//Cheep ass hack to use the above easily
		int		iRange;									//Maxium range for bullets to travel (also used for how far a melee hit goes)

		//Melee parses (TODO: Move into own parse set and implement Melee as they are in L4D2?)
		float	flReFireDelay;							//Refire delay for melee swings
		int		iDamageFlags;							//Damage type to apply on melee swing
		float	flWeaponIdleTime;						//Time before idle after a melee swing

		//CSPORT
		bool	m_bAccuracyQuadratic;
		float	m_flAccuracyDivisor;
		float	m_flAccuracyOffset;
		float	m_flMaxInaccuracy;

		//new parses
		bool	bUseL4DDrawAct;							//If true, use ACT_VM_DEPLOY. If false, use ACT_VM_DRAW.
};

#endif