
//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "in_buttons.h"

#include "l4dce_player.h"

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CWeaponHolster C_WeaponHolster
#endif

//-----------------------------------------------------------------------------
// CWeaponHolster
//-----------------------------------------------------------------------------

class CWeaponHolster : public CTerrorWeaponBase
{
	DECLARE_CLASS( CWeaponHolster, CTerrorWeaponBase );
public:

	CWeaponHolster();
	void PrimaryAttack();
	void MeleeAttack();
	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

	DECLARE_ACTTABLE();

private:
	
	CWeaponHolster( const CWeaponHolster & );
};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponHolster, DT_WeaponHolster )

BEGIN_NETWORK_TABLE( CWeaponHolster, DT_WeaponHolster )
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CWeaponHolster )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_holster, CWeaponHolster );
PRECACHE_WEAPON_REGISTER( weapon_holster );

acttable_t CWeaponHolster::m_acttable[] = 
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_FRYINGPAN, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_FRYINGPAN, false },
	{ ACT_MP_RUN,			ACT_RUN_FRYINGPAN, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_FRYINGPAN, false },
	{ ACT_MP_JUMP,			ACT_JUMP_FRYINGPAN, false },
};


IMPLEMENT_ACTTABLE( CWeaponHolster );

void CWeaponHolster::PrimaryAttack()
{ return; }

void CWeaponHolster::MeleeAttack()
{ return; }

CWeaponHolster::CWeaponHolster()
{}
