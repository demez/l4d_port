#include "cbase.h"
#include "weapon_terror_melee_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponMeleeCricket C_WeaponMeleeCricket
#endif

class CWeaponMeleeCricket : public CTerrorWeaponMeleeBase
{
private:
	DECLARE_CLASS(CWeaponMeleeCricket, CTerrorWeaponMeleeBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:

};

BEGIN_NETWORK_TABLE(CWeaponMeleeCricket, DT_WeaponMeleeCricket)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_melee_cricket, CWeaponMeleeCricket);
PRECACHE_WEAPON_REGISTER(weapon_melee_cricket);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponMeleeCricket, DT_WeaponMeleeCricket);

BEGIN_PREDICTION_DATA(CWeaponMeleeCricket)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponMeleeCricket)
END_DATADESC();

