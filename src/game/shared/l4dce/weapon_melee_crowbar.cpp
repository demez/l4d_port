#include "cbase.h"
#include "weapon_terror_melee_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponMeleeCrowbar C_WeaponMeleeCrowbar
#endif

class CWeaponMeleeCrowbar : public CTerrorWeaponMeleeBase
{
private:
	DECLARE_CLASS(CWeaponMeleeCrowbar, CTerrorWeaponMeleeBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:

};

BEGIN_NETWORK_TABLE(CWeaponMeleeCrowbar, DT_WeaponMeleeCrowbar)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_melee_crowbar, CWeaponMeleeCrowbar);
PRECACHE_WEAPON_REGISTER(weapon_melee_crowbar);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponMeleeCrowbar, DT_WeaponMeleeCrowbar);

BEGIN_PREDICTION_DATA(CWeaponMeleeCrowbar)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponMeleeCrowbar)
END_DATADESC();

