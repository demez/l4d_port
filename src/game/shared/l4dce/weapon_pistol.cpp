#include "cbase.h"
#include "weapon_terror_pistol_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponPistol C_WeaponPistol
#endif

class CWeaponPistol : public CTerrorWeaponPistolBase
{
	private:
		DECLARE_CLASS(CWeaponPistol, CTerrorWeaponPistolBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponPistol, DT_WeaponPistol)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_pistol, CWeaponPistol);
PRECACHE_WEAPON_REGISTER(weapon_pistol);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponPistol, DT_WeaponPistol);

BEGIN_PREDICTION_DATA(CWeaponPistol)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponPistol)
END_DATADESC();

