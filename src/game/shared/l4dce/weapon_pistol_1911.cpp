#include "cbase.h"
#include "weapon_terror_pistol_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponPistol1911 C_WeaponPistol1911
#endif

class CWeaponPistol1911 : public CTerrorWeaponPistolBase
{
	private:
		DECLARE_CLASS(CWeaponPistol1911, CTerrorWeaponPistolBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
};

BEGIN_NETWORK_TABLE(CWeaponPistol1911, DT_WeaponPistol1911)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_pistol_1911, CWeaponPistol1911);
PRECACHE_WEAPON_REGISTER(weapon_pistol_1911);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponPistol1911, DT_WeaponPistol1911);

BEGIN_PREDICTION_DATA(CWeaponPistol1911)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponPistol1911)
END_DATADESC();
