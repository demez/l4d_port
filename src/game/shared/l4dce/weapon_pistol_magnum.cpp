#include "cbase.h"
#include "weapon_terror_pistol_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponPistolMagnum C_WeaponPistolMagnum
#endif

class CWeaponPistolMagnum : public CTerrorWeaponPistolBase
{
private:
	DECLARE_CLASS(CWeaponPistolMagnum, CTerrorWeaponPistolBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:

};

BEGIN_NETWORK_TABLE(CWeaponPistolMagnum, DT_WeaponPistolMagnum)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_pistol_magnum, CWeaponPistolMagnum);
PRECACHE_WEAPON_REGISTER(weapon_pistol_magnum);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponPistolMagnum, DT_WeaponPistolMagnum);

BEGIN_PREDICTION_DATA(CWeaponPistolMagnum)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponPistolMagnum)
END_DATADESC();
