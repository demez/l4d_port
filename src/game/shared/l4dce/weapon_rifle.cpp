#include "cbase.h"
#include "weapon_terror_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponRifle C_WeaponRifle
#endif

class CWeaponRifle : public CTerrorWeaponBase
{
	private:
		DECLARE_CLASS(CWeaponRifle, CTerrorWeaponBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_ACTTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponRifle, DT_WeaponRifle)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_rifle, CWeaponRifle);
PRECACHE_WEAPON_REGISTER(weapon_rifle);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponRifle, DT_WeaponRifle);

BEGIN_PREDICTION_DATA(CWeaponRifle)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponRifle)
END_DATADESC();

acttable_t CWeaponRifle::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_RIFLE, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_RIFLE, false },
	{ ACT_MP_RUN,			ACT_RUN_RIFLE, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_RIFLE, false },
	{ ACT_MP_JUMP,			ACT_JUMP_RIFLE, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_RIFLE, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_RIFLE, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_RIFLE, false },
};
IMPLEMENT_ACTTABLE(CWeaponRifle);
