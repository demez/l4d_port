#include "cbase.h"
#include "weapon_terror_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponRifleAK47 C_WeaponRifleAK47
#endif

class CWeaponRifleAK47 : public CTerrorWeaponBase
{
	private:
		DECLARE_CLASS(CWeaponRifleAK47, CTerrorWeaponBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_ACTTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponRifleAK47, DT_WeaponRifleAK47)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_rifle_ak47, CWeaponRifleAK47);
PRECACHE_WEAPON_REGISTER(weapon_rifle_ak47);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponRifleAK47, DT_WeaponRifleAK47);

BEGIN_PREDICTION_DATA(CWeaponRifleAK47)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponRifleAK47)
END_DATADESC();

acttable_t CWeaponRifleAK47::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_RIFLE, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_RIFLE, false },
	{ ACT_MP_RUN,			ACT_RUN_RIFLE, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_RIFLE, false },
	{ ACT_MP_JUMP,			ACT_JUMP_RIFLE, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_RIFLE, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_RIFLE, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_RIFLE, false },
};
IMPLEMENT_ACTTABLE(CWeaponRifleAK47);
