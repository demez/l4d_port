#include "cbase.h"
#include "weapon_terror_3rndburst_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponRifleDesert C_WeaponRifleDesert
#endif

class CWeaponRifleDesert : public CTerrorWeapon3rndBurstBase
{
	private:
		DECLARE_CLASS(CWeaponRifleDesert, CTerrorWeapon3rndBurstBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_ACTTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponRifleDesert, DT_WeaponRifleDesert)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_rifle_desert, CWeaponRifleDesert);
PRECACHE_WEAPON_REGISTER(weapon_rifle_desert);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponRifleDesert, DT_WeaponRifleDesert);

BEGIN_PREDICTION_DATA(CWeaponRifleDesert)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponRifleDesert)
END_DATADESC();

acttable_t CWeaponRifleDesert::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_RIFLE, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_RIFLE, false },
	{ ACT_MP_RUN,			ACT_RUN_RIFLE, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_RIFLE, false },
	{ ACT_MP_JUMP,			ACT_JUMP_RIFLE, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_RIFLE, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_RIFLE, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_RIFLE, false },
};
IMPLEMENT_ACTTABLE(CWeaponRifleDesert);

