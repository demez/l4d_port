#include "cbase.h"
#include "weapon_terror_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSMG C_WeaponSMG
#endif

class CWeaponSMG : public CTerrorWeaponBase
{
	private:
		DECLARE_CLASS(CWeaponSMG, CTerrorWeaponBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

LINK_ENTITY_TO_CLASS(weapon_smg, CWeaponSMG);
PRECACHE_WEAPON_REGISTER(weapon_smg);

BEGIN_NETWORK_TABLE(CWeaponSMG, DT_WeaponSMG)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSMG, DT_WeaponSMG);

BEGIN_PREDICTION_DATA(CWeaponSMG)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSMG)
END_DATADESC();
