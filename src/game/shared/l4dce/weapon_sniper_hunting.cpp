#include "cbase.h"
#include "weapon_terror_scope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSniperHunting C_WeaponSniperHunting
#endif

class CWeaponSniperHunting : public CTerrorWeaponScopeBase
{
	private:
		DECLARE_CLASS(CWeaponSniperHunting, CTerrorWeaponScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		void Spawn();
};

void CWeaponSniperHunting::Spawn()
{
	SetClassname("weapon_sniper_hunting"); // hack to allow for old names
	BaseClass::Spawn();
}

BEGIN_NETWORK_TABLE(CWeaponSniperHunting, DT_WeaponSniperHunting)
END_NETWORK_TABLE();

#ifndef CLIENT_DLL
LINK_ENTITY_TO_CLASS(weapon_hunting_rifle, CWeaponSniperHunting);
#endif

LINK_ENTITY_TO_CLASS(weapon_sniper_hunting, CWeaponSniperHunting);
PRECACHE_WEAPON_REGISTER(weapon_sniper_hunting);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSniperHunting, DT_WeaponSniperHunting);

BEGIN_PREDICTION_DATA(CWeaponSniperHunting)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSniperHunting)
END_DATADESC();

