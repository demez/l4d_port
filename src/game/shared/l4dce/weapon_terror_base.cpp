#include "cbase.h"
#include "weapon_terror_base.h"
#include "l4dce_playeranimstate.h"
#include "l4dce_viewmodel.h"

#ifndef CLIENT_DLL
#include "te_effect_dispatch.h"
#endif

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

LINK_ENTITY_TO_CLASS(terror_base_weapon, CTerrorWeaponBase);
PRECACHE_WEAPON_REGISTER(terror_base_weapon);

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorWeaponBase, DT_TerrorWeaponBase);

BEGIN_NETWORK_TABLE(CTerrorWeaponBase, DT_TerrorWeaponBase)
END_NETWORK_TABLE();

BEGIN_DATADESC(CTerrorWeaponBase)
END_DATADESC();

BEGIN_PREDICTION_DATA(CTerrorWeaponBase)
END_PREDICTION_DATA();

CTerrorWeaponBase::CTerrorWeaponBase()
	: BaseClass()
{
	SetPredictionEligible(true);
	#ifdef CLIENT_DLL
	SetPredictable(true);
	#endif

	m_bPickedUpAlready = false;

	m_fMaxRange1 = GetTerrorWpnData().iRange;
}

acttable_t CTerrorWeaponBase::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_SMG, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_SMG, false },
	{ ACT_MP_RUN,			ACT_RUN_SMG, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_SMG, false },
	{ ACT_MP_JUMP,			ACT_JUMP_SMG, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_SMG, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_SMG, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_SMG, false },
};
IMPLEMENT_ACTTABLE(CTerrorWeaponBase);

#ifdef CLIENT_DLL
bool CTerrorWeaponBase::ShouldPredict()
{
	if (GetOwner() && GetOwner() == C_BasePlayer::GetLocalPlayer())
		return true;

	return BaseClass::ShouldPredict();
}
#endif

bool CTerrorWeaponBase::IsPredicted() const
{
	return true;
}

const CTerrorWeaponInfo &CTerrorWeaponBase::GetTerrorWpnData() const
{
	return static_cast<const CTerrorWeaponInfo &>(GetWpnData());
}

CTerrorPlayer *CTerrorWeaponBase::GetPlayerOwner() const
{
	return static_cast<CTerrorPlayer *>(GetOwner());
}

Activity CTerrorWeaponBase::GetDrawActivity()
{
	return ACT_VM_DEPLOY;
}

Activity CTerrorWeaponBase::GetMeleeAttackActivity()
{
	return ACT_VM_MELEE;
}

void CTerrorWeaponBase::PrimaryAttack()
{
	// If my clip is empty (and I use clips) start reload
	if(UsesClipsForAmmo1() && !m_iClip1) {
		Reload();
		return;
	}

	// Only the player fires this way so we can cast
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if ( !pPlayer )
		return;

	pPlayer->DoMuzzleFlash();

	SendWeaponAnim(GetPrimaryAttackActivity());

	// player "shoot" animation
	//pPlayer->SetAnimation(PLAYER_ATTACK1);
	pPlayer->DoAnimationEvent(PLAYERANIMEVENT_ATTACK_PRIMARY);

	FireBulletsInfo_t info;
	info.m_vecSrc = pPlayer->Weapon_ShootPosition();

	info.m_vecDirShooting = pPlayer->GetAutoaimVector(AUTOAIM_SCALE_DEFAULT);

	// To make the firing framerate independent, we may have to fire more than one bullet here on low-framerate systems, 
	// especially if the weapon we're firing has a really fast rate of fire.
	info.m_iShots = 0;
	float fireRate = GetFireRate();

	while(m_flNextPrimaryAttack <= gpGlobals->curtime) {
		// MUST call sound before removing a round from the clip of a CMachineGun
		WeaponSound(SINGLE, m_flNextPrimaryAttack);
		m_flNextPrimaryAttack = m_flNextPrimaryAttack + fireRate;
		info.m_iShots++;
		if(!fireRate)
			break;
	}

	// Make sure we don't fire more than the amount in the clip
	if(UsesClipsForAmmo1()) {
		info.m_iShots = MIN(info.m_iShots, m_iClip1);
		m_iClip1 -= info.m_iShots;
	} else {
		info.m_iShots = MIN(info.m_iShots, pPlayer->GetAmmoCount(m_iPrimaryAmmoType));
		pPlayer->RemoveAmmo(info.m_iShots, m_iPrimaryAmmoType);
	}

	info.m_flDistance = MAX_TRACE_LENGTH;
	info.m_iAmmoType = m_iPrimaryAmmoType;
	info.m_iTracerFreq = 2;

	#if !defined( CLIENT_DLL )
	// Fire the bullets
	info.m_vecSpread = pPlayer->GetAttackSpread(this);
	#else
	//!!!HACKHACK - what does the client want this function for? 
	info.m_vecSpread = pPlayer->GetActiveWeapon()->GetBulletSpread();
	#endif // CLIENT_DLL

	float dmg = GetTerrorWpnData().flDamage;
	info.m_flDamage = dmg;
	info.m_flPlayerDamage = dmg;

	
	/*const CTerrorWeaponInfo &pCSInfo = GetTerrorWpnData();
	// These modifications feed back into flSpread eventually.
	if ( pCSInfo.m_flAccuracyDivisor != -1 )
	{
		int iShotsFired = pPlayer->m_iShotsFired;

		if ( pCSInfo.m_bAccuracyQuadratic )
			iShotsFired = iShotsFired * iShotsFired;
		else
			iShotsFired = iShotsFired * iShotsFired * iShotsFired;

		m_flAccuracy = ( iShotsFired / pCSInfo.m_flAccuracyDivisor) + pCSInfo.m_flAccuracyOffset;
		
		if (m_flAccuracy > pCSInfo.m_flMaxInaccuracy)
			m_flAccuracy = pCSInfo.m_flMaxInaccuracy;
	}*/

	pPlayer->FireBullets(info);

	if(!m_iClip1 && pPlayer->GetAmmoCount(m_iPrimaryAmmoType) <= 0) {
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}

	//Add our view kick in
	AddViewKick();
}

void CTerrorWeaponBase::SecondaryAttack()
{
	//This is for base classes to implement if it needs a secondary attack, like zoom/scope
	return;
}

void CTerrorWeaponBase::MeleeAttack()
{
	ShoveSwing(false);
}

bool CTerrorWeaponBase::Reload()
{
	return DefaultReload(GetMaxClip1(), GetMaxClip2(), ACT_VM_RELOAD);
}

void CTerrorWeaponBase::SetPickupTouch()
{
	if (g_bIsPlayerInSpawn)
		BaseClass::SetPickupTouch();
	else
		SetTouch(NULL); //aka requires +USE to be able to be picked up
}

void CTerrorWeaponBase::ItemPostFrame()
{
	CTerrorPlayer *pOwner = ToTerrorPlayer(GetOwner());
	if (!pOwner)
		return;

	//Track the duration of the fire
	//FIXME: Check for IN_ATTACK2 as well?
	//FIXME: What if we're calling ItemBusyFrame?
	m_fFireDuration = ( pOwner->m_nButtons & IN_ATTACK ) ? ( m_fFireDuration + gpGlobals->frametime ) : 0.0f;

	if ( UsesClipsForAmmo1() )
	{
		CheckReload();
	}

	bool bFired = false;

	// Secondary attack has priority
	if ((pOwner->m_nButtons & IN_ATTACK2) && (m_flNextSecondaryAttack <= gpGlobals->curtime))
	{
		if (UsesSecondaryAmmo() && pOwner->GetAmmoCount(m_iSecondaryAmmoType)<=0 )
		{
			if (m_flNextEmptySoundTime < gpGlobals->curtime)
			{
				WeaponSound(EMPTY);
				m_flNextSecondaryAttack = m_flNextEmptySoundTime = gpGlobals->curtime + 0.5;
			}
		}
		else if (pOwner->GetWaterLevel() == 3 && m_bAltFiresUnderwater == false)
		{
			// This weapon doesn't fire underwater
			WeaponSound(EMPTY);
			m_flNextPrimaryAttack = gpGlobals->curtime + 0.2;
			return;
		}
		else
		{
			// FIXME: This isn't necessarily true if the weapon doesn't have a secondary fire!
			// For instance, the crossbow doesn't have a 'real' secondary fire, but it still 
			// stops the crossbow from firing on the 360 if the player chooses to hold down their
			// zoom button. (sjb) Orange Box 7/25/2007
#if !defined(CLIENT_DLL)
			if( !IsX360() || !ClassMatches("weapon_crossbow") )
#endif
			{
				bFired = true;
			}

			SecondaryAttack();

			// Secondary ammo doesn't have a reload animation
			if ( UsesClipsForAmmo2() )
			{
				// reload clip2 if empty
				if (m_iClip2 < 1)
				{
					pOwner->RemoveAmmo( 1, m_iSecondaryAmmoType );
					m_iClip2 = m_iClip2 + 1;
				}
			}
		}
	}
	
	if ( !bFired && (pOwner->m_nButtons & IN_ATTACK) && (m_flNextPrimaryAttack <= gpGlobals->curtime))
	{
		// Clip empty? Or out of ammo on a no-clip weapon?
		if ( !IsMeleeWeapon() &&  
			(( UsesClipsForAmmo1() && m_iClip1 <= 0) || ( !UsesClipsForAmmo1() && pOwner->GetAmmoCount(m_iPrimaryAmmoType)<=0 )) )
		{
			HandleFireOnEmpty();
		}
		else if (pOwner->GetWaterLevel() == 3 && m_bFiresUnderwater == false)
		{
			// This weapon doesn't fire underwater
			WeaponSound(EMPTY);
			m_flNextPrimaryAttack = gpGlobals->curtime + 0.2;
			return;
		}
		else
		{
			//NOTENOTE: There is a bug with this code with regards to the way machine guns catch the leading edge trigger
			//			on the player hitting the attack key.  It relies on the gun catching that case in the same frame.
			//			However, because the player can also be doing a secondary attack, the edge trigger may be missed.
			//			We really need to hold onto the edge trigger and only clear the condition when the gun has fired its
			//			first shot.  Right now that's too much of an architecture change -- jdw
			
			// If the firing button was just pressed, or the alt-fire just released, reset the firing time
			if ( ( pOwner->m_afButtonPressed & IN_ATTACK ) || ( pOwner->m_afButtonReleased & IN_ATTACK2 ) )
			{
				 m_flNextPrimaryAttack = gpGlobals->curtime;
			}

			PrimaryAttack();
		}
	}

	if ( !bFired && (pOwner->m_nButtons & IN_MELEE ) && (m_flNextMeleeAttack <= gpGlobals->curtime))
	{
		if ( ( pOwner->m_afButtonPressed & IN_MELEE ) )
		{
			 m_flNextMeleeAttack = gpGlobals->curtime;
		}

		MeleeAttack();
	}

	// -----------------------
	//  Reload pressed / Clip Empty
	// -----------------------
	if ( (pOwner->m_nButtons & IN_RELOAD) && UsesClipsForAmmo1() && !m_bInReload ) 
	{
		// reload when reload is pressed, or if no buttons are down and weapon is empty.
		Reload();
		m_fFireDuration = 0.0f;
	}

	// -----------------------
	//  No buttons down
	// -----------------------
	if (!((pOwner->m_nButtons & IN_ATTACK) || (pOwner->m_nButtons & IN_ATTACK2) || (pOwner->m_nButtons & IN_RELOAD)))
	{
		// no fire buttons down or reloading
		if ( ( m_bInReload == false ) && !ReloadOrSwitchWeapons() )
		{
			WeaponIdle();
		}
	}
}


#ifdef CLIENT_DLL
#define	HL2_BOB_CYCLE_MIN	1.0f
#define	HL2_BOB_CYCLE_MAX	0.45f
#define	HL2_BOB			0.002f
#define	HL2_BOB_UP		0.5f

float	g_lateralBob;
float	g_verticalBob;

static ConVar	cl_bobcycle("cl_bobcycle", "0.8");
static ConVar	cl_bob("cl_bob", "0.002");
static ConVar	cl_bobup("cl_bobup", "0.5");

// Register these cvars if needed for easy tweaking
static ConVar	v_iyaw_cycle("v_iyaw_cycle", "2", FCVAR_REPLICATED | FCVAR_CHEAT);
static ConVar	v_iroll_cycle("v_iroll_cycle", "0.5", FCVAR_REPLICATED | FCVAR_CHEAT);
static ConVar	v_ipitch_cycle("v_ipitch_cycle", "1", FCVAR_REPLICATED | FCVAR_CHEAT);
static ConVar	v_iyaw_level("v_iyaw_level", "0.3", FCVAR_REPLICATED | FCVAR_CHEAT);
static ConVar	v_iroll_level("v_iroll_level", "0.1", FCVAR_REPLICATED | FCVAR_CHEAT);
static ConVar	v_ipitch_level("v_ipitch_level", "0.3", FCVAR_REPLICATED | FCVAR_CHEAT);

float CTerrorWeaponBase::CalcViewmodelBob(void)
{
	static	float bobtime;
	static	float lastbobtime;
	float	cycle;

	CBasePlayer *player = ToBasePlayer(GetOwner());
	//Assert( player );

	//NOTENOTE: For now, let this cycle continue when in the air, because it snaps badly without it

	if((!gpGlobals->frametime) || (player == NULL)) {
		//NOTENOTE: We don't use this return value in our case (need to restructure the calculation function setup!)
		return 0.0f;// just use old value
	}

	//Find the speed of the player
	float speed = player->GetLocalVelocity().Length2D();

	//FIXME: This maximum speed value must come from the server.
	//		 MaxSpeed() is not sufficient for dealing with sprinting - jdw

	speed = clamp(speed, -320, 320);

	float bob_offset = RemapVal(speed, 0, 320, 0.0f, 1.0f);

	bobtime += (gpGlobals->curtime - lastbobtime) * bob_offset;
	lastbobtime = gpGlobals->curtime;

	//Calculate the vertical bob
	cycle = bobtime - (int)(bobtime / HL2_BOB_CYCLE_MAX)*HL2_BOB_CYCLE_MAX;
	cycle /= HL2_BOB_CYCLE_MAX;

	if(cycle < HL2_BOB_UP) {
		cycle = M_PI * cycle / HL2_BOB_UP;
	} else {
		cycle = M_PI + M_PI * (cycle - HL2_BOB_UP) / (1.0 - HL2_BOB_UP);
	}

	g_verticalBob = speed * 0.005f;
	g_verticalBob = g_verticalBob * 0.3 + g_verticalBob * 0.7*sin(cycle);

	g_verticalBob = clamp(g_verticalBob, -7.0f, 4.0f);

	//Calculate the lateral bob
	cycle = bobtime - (int)(bobtime / HL2_BOB_CYCLE_MAX * 2)*HL2_BOB_CYCLE_MAX * 2;
	cycle /= HL2_BOB_CYCLE_MAX * 2;

	if(cycle < HL2_BOB_UP) {
		cycle = M_PI * cycle / HL2_BOB_UP;
	} else {
		cycle = M_PI + M_PI * (cycle - HL2_BOB_UP) / (1.0 - HL2_BOB_UP);
	}

	g_lateralBob = speed * 0.005f;
	g_lateralBob = g_lateralBob * 0.3 + g_lateralBob * 0.7*sin(cycle);
	g_lateralBob = clamp(g_lateralBob, -7.0f, 4.0f);

	//NOTENOTE: We don't use this return value in our case (need to restructure the calculation function setup!)
	return 0.0f;
}

void CTerrorWeaponBase::AddViewmodelBob(CBaseViewModel *viewmodel, Vector &origin, QAngle &angles)
{
	Vector	forward, right;
	AngleVectors(angles, &forward, &right, NULL);

	CalcViewmodelBob();

	// Apply bob, but scaled down to 40%
	VectorMA(origin, g_verticalBob * 0.1f, forward, origin);

	// Z bob a bit more
	origin[2] += g_verticalBob * 0.1f;

	// bob the angles
	angles[ROLL] += g_verticalBob * 0.5f;
	angles[PITCH] -= g_verticalBob * 0.4f;

	angles[YAW] -= g_lateralBob * 0.3f;

	VectorMA(origin, g_lateralBob * 0.8f, right, origin);
}
#else
float CTerrorWeaponBase::CalcViewmodelBob(void)
{
	return 0.0f;
}

void CTerrorWeaponBase::AddViewmodelBob(CBaseViewModel *viewmodel, Vector &origin, QAngle &angles)
{

}
#endif

const char *CTerrorWeaponBase::GetViewModel(int iViewModel) const
{
	return GetTerrorWpnData().szViewModel;
}

float CTerrorWeaponBase::GetFireRate()
{
	return GetTerrorWpnData().flCycleTime;
}

const Vector &CTerrorWeaponBase::GetBulletSpread()
{
	static const Vector vecCone = GetTerrorWpnData().vecSpreadPerShot;
	return vecCone;
}

void CTerrorWeaponBase::Equip(CBaseCombatCharacter *pOwner)
{
	BaseClass::Equip(pOwner);

#ifndef CLIENT_DLL 
	CTerrorPlayer *pPlayer = ToTerrorPlayer(pOwner);
	if (!pPlayer)
		return;

	if (!m_bPickedUpAlready)
		pPlayer->GiveAmmo(999, GetTerrorWpnData().szAmmo1, true);
#endif 


	m_bPickedUpAlready = true;
}

float CTerrorWeaponBase::ShoveGetRange()
{
	return	32.0f;
}

#define MELEE_BUTT_DAMAGE 20.0f

//------------------------------------------------------------------------------
// Purpose: Implement impact function
//------------------------------------------------------------------------------
void CTerrorWeaponBase::ShoveHit( trace_t &traceHit, Activity nHitActivity )
{
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );
	
	//Do view kick
	AddViewKick();

	CBaseEntity	*pHitEntity = traceHit.m_pEnt;

	//Apply damage to a hit target
	if ( pHitEntity != NULL )
	{
		Vector hitDirection;
		pPlayer->EyeVectors( &hitDirection, NULL, NULL );
		VectorNormalize( hitDirection );

#ifndef CLIENT_DLL
		CTakeDamageInfo info( GetOwner(), GetOwner(), MELEE_BUTT_DAMAGE, DMG_CLUB );

		if( pPlayer && pHitEntity->IsNPC() )
		{
			// If bonking an NPC, adjust damage.
			info.AdjustPlayerDamageInflictedForSkillLevel();
		}

		CalculateMeleeDamageForce( &info, hitDirection, traceHit.endpos );

		pHitEntity->DispatchTraceAttack( info, hitDirection, &traceHit ); 
		ApplyMultiDamage();

		// Now hit all triggers along the ray that... 
		TraceAttackToTriggers( info, traceHit.startpos, traceHit.endpos, hitDirection );
#endif
		WeaponSound( MELEE_HIT );
	}

	// Apply an impact effect
	ShoveImpactEffect( traceHit );
}

Activity CTerrorWeaponBase::ShoveChooseIntersectionPointAndActivity( trace_t &hitTrace, const Vector &mins, const Vector &maxs, CBasePlayer *pOwner )
{
	int			i, j, k;
	float		distance;
	const float	*minmaxs[2] = {mins.Base(), maxs.Base()};
	trace_t		tmpTrace;
	Vector		vecHullEnd = hitTrace.endpos;
	Vector		vecEnd;

	distance = 1e6f;
	Vector vecSrc = hitTrace.startpos;

	vecHullEnd = vecSrc + ((vecHullEnd - vecSrc)*2);
	UTIL_TraceLine( vecSrc, vecHullEnd, MASK_SHOT_HULL, pOwner, COLLISION_GROUP_NONE, &tmpTrace );
	if ( tmpTrace.fraction == 1.0 )
	{
		for ( i = 0; i < 2; i++ )
		{
			for ( j = 0; j < 2; j++ )
			{
				for ( k = 0; k < 2; k++ )
				{
					vecEnd.x = vecHullEnd.x + minmaxs[i][0];
					vecEnd.y = vecHullEnd.y + minmaxs[j][1];
					vecEnd.z = vecHullEnd.z + minmaxs[k][2];

					UTIL_TraceLine( vecSrc, vecEnd, MASK_SHOT_HULL, pOwner, COLLISION_GROUP_NONE, &tmpTrace );
					if ( tmpTrace.fraction < 1.0 )
					{
						float thisDistance = (tmpTrace.endpos - vecSrc).Length();
						if ( thisDistance < distance )
						{
							hitTrace = tmpTrace;
							distance = thisDistance;
						}
					}
				}
			}
		}
	}
	else
	{
		hitTrace = tmpTrace;
	}


	return ACT_VM_HITCENTER;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &traceHit - 
//-----------------------------------------------------------------------------
bool CTerrorWeaponBase::ShoveImpactWater( const Vector &start, const Vector &end )
{
	//FIXME: This doesn't handle the case of trying to splash while being underwater, but that's not going to look good
	//		 right now anyway...
	
	// We must start outside the water
	if ( UTIL_PointContents( start, (CONTENTS_WATER|CONTENTS_SLIME)))
		return false;

	// We must end inside of water
	if ( !(UTIL_PointContents( end, (CONTENTS_WATER|CONTENTS_SLIME))))
		return false;

	trace_t	waterTrace;

	UTIL_TraceLine( start, end, (CONTENTS_WATER|CONTENTS_SLIME), GetOwner(), COLLISION_GROUP_NONE, &waterTrace );

	if ( waterTrace.fraction < 1.0f )
	{
#ifndef CLIENT_DLL
		CEffectData	data;

		data.m_fFlags  = 0;
		data.m_vOrigin = waterTrace.endpos;
		data.m_vNormal = waterTrace.plane.normal;
		data.m_flScale = 8.0f;

		// See if we hit slime
		if ( waterTrace.contents & CONTENTS_SLIME )
		{
			data.m_fFlags |= FX_WATER_IN_SLIME;
		}

		DispatchEffect( "watersplash", data );			
#endif
	}

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CTerrorWeaponBase::ShoveImpactEffect( trace_t &traceHit )
{
	// See if we hit water (we don't do the other impact effects in this case)
	if ( ShoveImpactWater( traceHit.startpos, traceHit.endpos ) )
		return;

	//FIXME: need new decals
	UTIL_ImpactTrace( &traceHit, DMG_CLUB );
}

#define BLUDGEON_HULL_DIM		16
static const Vector g_bludgeonMins(-BLUDGEON_HULL_DIM,-BLUDGEON_HULL_DIM,-BLUDGEON_HULL_DIM);
static const Vector g_bludgeonMaxs(BLUDGEON_HULL_DIM,BLUDGEON_HULL_DIM,BLUDGEON_HULL_DIM);

//------------------------------------------------------------------------------
// Purpose : Starts the swing of the weapon and determines the animation
// Input   : bIsSecondary - is this a secondary attack?
//------------------------------------------------------------------------------
void CTerrorWeaponBase::ShoveSwing( int bIsSecondary )
{
	trace_t traceHit;

	// Try a ray
	CBasePlayer *pOwner = ToBasePlayer( GetOwner() );
	if ( !pOwner )
		return;

	Vector swingStart = pOwner->Weapon_ShootPosition( );
	Vector forward;

	pOwner->EyeVectors( &forward, NULL, NULL );

	Vector swingEnd = swingStart + forward * ShoveGetRange();
	UTIL_TraceLine( swingStart, swingEnd, MASK_SHOT_HULL, pOwner, COLLISION_GROUP_NONE, &traceHit );
	Activity nHitActivity = ACT_VM_MELEE;

#ifndef CLIENT_DLL
	// Like bullets, bludgeon traces have to trace against triggers.
	CTakeDamageInfo triggerInfo( GetOwner(), GetOwner(), MELEE_BUTT_DAMAGE, DMG_CLUB );
	TraceAttackToTriggers( triggerInfo, traceHit.startpos, traceHit.endpos, vec3_origin );
#endif

	if ( traceHit.fraction == 1.0 )
	{
		float bludgeonHullRadius = 1.732f * BLUDGEON_HULL_DIM;  // hull is +/- 16, so use cuberoot of 2 to determine how big the hull is from center to the corner point

		// Back off by hull "radius"
		swingEnd -= forward * bludgeonHullRadius;

		UTIL_TraceHull( swingStart, swingEnd, g_bludgeonMins, g_bludgeonMaxs, MASK_SHOT_HULL, pOwner, COLLISION_GROUP_NONE, &traceHit );
		if ( traceHit.fraction < 1.0 && traceHit.m_pEnt )
		{
			Vector vecToTarget = traceHit.m_pEnt->GetAbsOrigin() - swingStart;
			VectorNormalize( vecToTarget );

			float dot = vecToTarget.Dot( forward );

			// YWB:  Make sure they are sort of facing the guy at least...
			if ( dot < 0.70721f )
			{
				// Force amiss
				traceHit.fraction = 1.0f;
			}
			else
			{
				nHitActivity = ShoveChooseIntersectionPointAndActivity( traceHit, g_bludgeonMins, g_bludgeonMaxs, pOwner );
			}
		}
	}

	// -------------------------
	//	Miss
	// -------------------------
	if ( traceHit.fraction == 1.0f )
	{

		// We want to test the first swing again
		Vector testEnd = swingStart + forward * ShoveGetRange();
		
		// See if we happened to hit water
		ShoveImpactWater( swingStart, testEnd );
	}
	else
	{
		ShoveHit( traceHit, nHitActivity );
	}

	// Send the anim
	SendWeaponAnim( nHitActivity );

	pOwner->SetAnimation( PLAYER_ATTACK1 );

	//Setup our next attack times
	m_flNextMeleeAttack = gpGlobals->curtime + SequenceDuration();
	m_flNextPrimaryAttack = gpGlobals->curtime + SequenceDuration();
	m_flNextSecondaryAttack = gpGlobals->curtime + SequenceDuration();
}

void CTerrorWeaponBase::SetWeaponVisible( bool visible )
{
    CBaseViewModel* vm = nullptr;
    CBaseViewModel* vmhands = nullptr;

    CTerrorPlayer* pOwner = GetPlayerOwner();
    if ( pOwner )
    {
        vm = pOwner->GetViewModel( VMINDEX_WEP );
        vmhands = pOwner->GetViewModel( VMINDEX_HANDS );

#ifndef CLIENT_DLL
        Assert( vm == pOwner->GetViewModel( m_nViewModelIndex ) );
#endif
    }

    if ( visible )
    {
        RemoveEffects( EF_NODRAW );

        if ( vm ) vm->RemoveEffects( EF_NODRAW );
        if ( vmhands ) vmhands->RemoveEffects( EF_NODRAW );
    }
    else
    {
        AddEffects( EF_NODRAW );

        if ( vm ) vm->AddEffects( EF_NODRAW );
        if ( vmhands ) vmhands->AddEffects( EF_NODRAW );
    }
}


bool CTerrorWeaponBase::Deploy()
{
    bool res = BaseClass::Deploy();

    if ( res )
    {
        CTerrorPlayer* pOwner = GetPlayerOwner();

        if ( pOwner )
        {
            CBaseViewModel* vmhands = pOwner->GetViewModel( VMINDEX_HANDS );

            if ( vmhands )
            {
                vmhands->RemoveEffects( EF_NODRAW );
            }
        }
    }

    return res;
}

bool CTerrorWeaponBase::Holster(CBaseCombatWeapon* pSwitchTo)
{
    bool res = BaseClass::Holster( pSwitchTo );

    return res;
}

#ifndef CLIENT_DLL
void CC_PrintCurrentSpread(void)
{
	CTerrorPlayer *pPlayer = ToTerrorPlayer(UTIL_GetCommandClient());
	if (!pPlayer)
		return;

	CTerrorWeaponBase *pWeapon = pPlayer->GetActiveTerrorWeapon();
	if (!pWeapon)
		return;

	Msg("CURRENT WEAPON SPREAD: %f\n", pWeapon->GetTerrorWpnData().flSpreadPerShot);
}
static ConCommand printcurrentspread("printcurrentspread", CC_PrintCurrentSpread, "printcurrentspread\n", FCVAR_DEVELOPMENTONLY);
#endif