#include "cbase.h"
#include "weapon_terror_pistol_base.h"
#include "tier0/memdbgon.h"
#include "in_buttons.h"


LINK_ENTITY_TO_CLASS(terror_base_weapon_pistol, CTerrorWeaponPistolBase);
PRECACHE_WEAPON_REGISTER(terror_base_weapon_pistol);

BEGIN_NETWORK_TABLE(CTerrorWeaponPistolBase, DT_TerrorWeaponPistolBase)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorWeaponPistolBase, DT_TerrorWeaponPistolBase);

BEGIN_PREDICTION_DATA(CTerrorWeaponPistolBase)
END_PREDICTION_DATA();

BEGIN_DATADESC(CTerrorWeaponPistolBase)
END_DATADESC();

acttable_t CTerrorWeaponPistolBase::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_PISTOL, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_PISTOL, false },
	{ ACT_MP_RUN,			ACT_RUN_PISTOL, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_PISTOL, false },
	{ ACT_MP_JUMP,			ACT_JUMP_PISTOL, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_PISTOL, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_PISTOL, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_PISTOL, false },
};
IMPLEMENT_ACTTABLE(CTerrorWeaponPistolBase);

void CTerrorWeaponPistolBase::PrimaryAttack()
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (!(pPlayer->m_afButtonPressed & IN_ATTACK))
		return;

	BaseClass::PrimaryAttack();
}

bool CTerrorWeaponPistolBase::Reload(void)
{
	if (m_iClip1 == 0)
		return DefaultReload(GetMaxClip1(), GetMaxClip2(), ACT_VM_RELOAD_EMPTY);
	else
		return DefaultReload(GetMaxClip1(), GetMaxClip2(), ACT_VM_RELOAD);
}
