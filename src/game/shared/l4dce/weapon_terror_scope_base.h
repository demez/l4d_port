#ifndef _TERROR_WEAPON_SCOPE_BASE_H_
#define _TERROR_WEAPON_SCOPE_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponScopeBase C_TerrorWeaponScopeBase
#endif

class CTerrorWeaponScopeBase : public CTerrorWeaponBase
{
private:
	DECLARE_CLASS(CTerrorWeaponScopeBase, CTerrorWeaponBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_ACTTABLE();
	DECLARE_DATADESC();

public:
	void PrimaryAttack();
};

#endif