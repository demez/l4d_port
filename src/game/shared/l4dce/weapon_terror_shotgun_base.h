#ifndef _TERROR_WEAPON_SHOTGUN_BASE_H_
#define _TERROR_WEAPON_SHOTGUN_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponShotgunBase C_TerrorWeaponShotgunBase
#endif

class CTerrorWeaponShotgunBase : public CTerrorWeaponBase
{
private:
	DECLARE_CLASS(CTerrorWeaponShotgunBase, CTerrorWeaponBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_ACTTABLE();
	DECLARE_DATADESC();

private:
	CNetworkVar(bool, m_bDelayedFire1);	// Fire primary when finished reloading
	CNetworkVar(bool, m_bDelayedFire2);	// Fire secondary when finished reloading
	CNetworkVar(bool, m_bDelayedReload);	// Reload when finished pump
	CNetworkVar(bool, m_bFirstShell);

public:

	bool StartReload(void);
	bool Reload(void);
	void FillClip(void);
	void FinishReload(void);
	void CheckHolsterReload(void);
	//	void WeaponIdle( void );
	void ItemHolsterFrame(void);
	void ItemPostFrame(void);
	void PrimaryAttack(void);
	void SecondaryAttack(void);
	void DryFire(void);
	virtual float GetFireRate(void) { return GetTerrorWpnData().flCycleTime; };

	CTerrorWeaponShotgunBase(void);

private:
	CTerrorWeaponShotgunBase(const CTerrorWeaponShotgunBase &);

public:
	bool m_bShotgunAutomatic;
	bool m_bShotgunPumpAction;
};

#endif