#include "cbase.h"
#include "weapon_terror_tactscope_base.h"
#include "tier0/memdbgon.h"
#include "in_buttons.h"


LINK_ENTITY_TO_CLASS(terror_base_weapon_tactscope, CTerrorWeaponTactScopeBase);
PRECACHE_WEAPON_REGISTER(terror_base_weapon_tactscope);

BEGIN_NETWORK_TABLE(CTerrorWeaponTactScopeBase, DT_TerrorWeaponTactScopeBase)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorWeaponTactScopeBase, DT_TerrorWeaponTactScopeBase);

BEGIN_PREDICTION_DATA(CTerrorWeaponTactScopeBase)
END_PREDICTION_DATA();

BEGIN_DATADESC(CTerrorWeaponTactScopeBase)
END_DATADESC();


CTerrorWeaponTactScopeBase::CTerrorWeaponTactScopeBase()
{
	m_bInZoom = false;
}

float CTerrorWeaponTactScopeBase::GetFireRate()
{
	if (m_bInZoom)
		return (GetTerrorWpnData().flCycleTime * 2); //half the rate when scoped
	else
		return GetTerrorWpnData().flCycleTime;
}

bool CTerrorWeaponTactScopeBase::Holster(CBaseCombatWeapon *pSwitchingTo)
{
	if (m_bInZoom)
		ToggleZoom();

	return BaseClass::Holster(pSwitchingTo);
}

void CTerrorWeaponTactScopeBase::ItemPostFrame()
{
	CheckZoomToggle();

	BaseClass::ItemPostFrame();
}

bool CTerrorWeaponTactScopeBase::Reload()
{
	if (m_bInZoom)
		ToggleZoom();

	return BaseClass::Reload();
}

void CTerrorWeaponTactScopeBase::ItemBusyFrame(void)
{
	CheckZoomToggle();

	BaseClass::ItemBusyFrame();
}

void CTerrorWeaponTactScopeBase::CheckZoomToggle(void)
{
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	if (pPlayer && (pPlayer->m_afButtonPressed & IN_ATTACK2))
	{
		ToggleZoom();
	}
}

void CTerrorWeaponTactScopeBase::ToggleZoom(void)
{
#ifndef CLIENT_DLL
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	if (pPlayer == NULL)
		return;

	if (m_bInZoom)
	{
		if (pPlayer->SetFOV(this, 0, 0.2f))
		{
			m_bInZoom = false;
			pPlayer->EmitSound("CS_Default.Zoom");
		}
	}
	else
	{
		if (pPlayer->SetFOV(this, 55, 0.1f))
		{
			m_bInZoom = true;
			pPlayer->EmitSound("CS_Default.Zoom");
		}
	}
#endif
}


void CTerrorWeaponTactScopeBase::MeleeAttack()
{
	if (m_bInZoom)
		ToggleZoom();

	BaseClass::MeleeAttack();
}
